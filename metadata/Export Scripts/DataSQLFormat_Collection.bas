Attribute VB_Name = "Export_Metadata_Collection"
Sub FillFunctionNames()
    'Write the title of the collection here.
    'CollectionTitle = "Tennyson's Maud"

    'This can just be the same as the file/folder name
    CollectionRefId = "bronte_sonnets"

    
    ShowColumnTitles = True
    sheetCount = 46
    RowGeneralInfo = 2
    RowPagesStart = 11
    
    'Source cells in the spreadsheet for a score
    SourceTitle = "B"
    SourceArtists = "D"
    'SourceDedicatee = "C2"
    SourcePublisher = "E"
    SourceDate = "F"
    SourceSubject = "H"
    SourceForm = "I"
    SourceKeySig = "J"
    SourceTimeSig = "K"
    SourceTempo = "L"
    SourceInstr = "M"
    'SourceCoverDesc = "M2"
    SourceNote = "V"
    SourceResourceIdentifier = "U"
    
    'Columns for the respective fields in the "Data" spreadsheet - the one to be exported.
    ColRefId = "A"
    ColTitle = "B"
    ColLyricist = "C"
    ColComposer = "D"
    ColAuthor = "E"
    'ColDedicatee = "F"
    ColPublisher = "F"
    ColDate = "G"
    ColSubject = "H"
    ColForm = "I"
    ColKeySig = "J"
    ColTimeSig = "K"
    ColTempo = "L"
    ColInstr = "M"
    'ColCoverDesc = "O"
    ColNote = "N"
    ColPageCount = "O"
    ColIndexStart = "P"
    'ColIndexEnd = "Q"
    'ColPageStart = "R"
    'ColCollectionTitle = "S"
    
    'RefId Array - user identifier for score
    'Dim ListRefId
    'ListRefId = Array()
    'ReDim ListRefId(sheetCount)
    
    'List of score titles
    Dim ListTitle
    ListTitle = Array()
    ReDim ListTitle(sheetCount)
    
    'List of lyricists
    Dim ListLyricist
    ListLyricist = Array()
    ReDim ListLyricist(sheetCount)
    
    'List of composers
    Dim ListComposer
    ListComposer = Array()
    ReDim ListComposer(sheetCount)
    
    'List of authors
    Dim ListAuthor
    ListAuthor = Array()
    ReDim ListAuthor(sheetCount)
    
    'List of publishers
    Dim ListPublisher
    ListPublisher = Array()
    ReDim ListPublisher(sheetCount)
    
    'Dim ListDedicatee
    'ListDedicatee = Array()
    'ReDim ListDedicatee(sheetCount)
    
    Dim ListDate
    ListDate = Array()
    ReDim ListDate(sheetCount)
    
    Dim ListSubject
    ListSubject = Array()
    ReDim ListSubject(sheetCount)
    
    'List of forms/genres
    Dim ListForm
    ListForm = Array()
    ReDim ListForm(sheetCount)
    
    'List of key signatures
    Dim ListKeySig
    ListKeySig = Array()
    ReDim ListKeySig(sheetCount)
    
    'List of time signatures
    Dim ListTimeSig
    ListTimeSig = Array()
    ReDim ListTimeSig(sheetCount)
    
    Dim ListTempo
    ListTempo = Array()
    ReDim ListTempo(sheetCount)
    
    'List of instrumentations
    Dim ListInstr
    ListInstr = Array()
    ReDim ListInstr(sheetCount)
    
    'List of cover descriptions
    Dim ListCoverDesc
    ListCoverDesc = Array()
    ReDim ListCoverDesc(sheetCount)
    
    Dim ListNote
    ListNote = Array()
    ReDim ListNote(sheetCount)
    
    Dim ListPageCount
    ListPageCount = Array()
    ReDim ListPageCount(sheetCount)
    
    Dim ListIndexStart
    ListIndexStart = Array()
    ReDim ListIndexStart(sheetCount)
    
    CollectionTitle = Range("C" & RowGeneralInfo)
    CollectionPublisher = Range("E" & RowGeneralInfo)
    CollectionDate = Range("F" & RowGeneralInfo)
    
    PrevTitle = "fdjapoeijrpouie" 'Stupidly written code, but it works...
    
    ScoreIndex = 0
    ScorePageCount = 0
    
    For i = RowPagesStart To sheetCount
        Dim listIndex
        listIndex = i
        
        'ListRefId(listIndex) = CollectionRefId
        CurTitle = Range(SourceTitle & i)
        
        If CurTitle <> PrevTitle Then
            If ScoreIndex <> 0 Then
                ListPageCount(ScoreIndex) = ScorePageCount
                ScorePageCount = 0
            End If
            PrevTitle = CurTitle
            ScoreIndex = ScoreIndex + 1
            ListIndexStart(ScoreIndex) = CInt(Right(Range(SourceResourceIdentifier & listIndex), 3)) 'TODO: UPDATE VALUE
        
            ListTitle(ScoreIndex) = CurTitle
            ListPublisher(ScoreIndex) = CollectionPublisher
            'ListDedicatee(listIndex) = Range(SourceDedicatee & RowPagesStart)
            ListDate(ScoreIndex) = CollectionDate
            ListSubject(ScoreIndex) = Range(SourceSubject & listIndex)
            ListForm(ScoreIndex) = Range(SourceForm & listIndex)
            ListKeySig(ScoreIndex) = Range(SourceKeySig & listIndex)
            ListTimeSig(ScoreIndex) = Range(SourceTimeSig & listIndex)
            ListTempo(ScoreIndex) = Range(SourceTempo & listIndex)
            ListInstr(ScoreIndex) = Replace(LCase(Replace(Range(SourceInstr & listIndex), " ", "")), ";", ",")
            'ListCoverDesc(listIndex) = Range(SourceCoverDesc & i)
            ListNote(ScoreIndex) = Range(SourceNote & listIndex)
            
            'One cell has the composer, lyricist, and author
            Dim contribList
            contribList = Split(Range(SourceArtists & listIndex), ";")
            
            For Each artist In contribList
                If InStr(artist, "(composer)") <> 0 Then
                    artist = Trim(Replace(artist, "(composer)", ""))
                    If ListComposer(listIndex) & "" <> "" Then
                        artist = ";" & artist
                    End If
                    ListComposer(ScoreIndex) = ListComposer(listIndex) & artist
                ElseIf InStr(artist, "(lyricist)") <> 0 Then
                    artist = Trim(Replace(artist, "(lyricist)", ""))
                    If ListLyricist(listIndex) & "" <> "" Then
                        artist = ";" & artist
                    End If
                    ListLyricist(ScoreIndex) = ListLyricist(listIndex) & artist
                ElseIf InStr(artist, "(author)") <> 0 Then
                    artist = Trim(Replace(artist, "(author)", ""))
                    If ListAuthor(listIndex) & "" <> "" Then
                        artist = ";" & artist
                    End If
                    ListAuthor(ScoreIndex) = ListAuthor(listIndex) & artist
                End If
                
            Next
        End If
        
        ScorePageCount = ScorePageCount + 1
        If i = sheetCount Then
            ListPageCount(ScoreIndex) = ScorePageCount
        End If
        
    Next
    
    Sheets.Add After:=Worksheets(1)
    Set NewSheet = Worksheets(2)
    NewSheet.Select
    NewSheet.Name = "Data"

    Dim rowStartIndex
    
    If ShowColumnTitles Then
        rowStartIndex = 1
        
        Range(ColRefId & 1).Select
        ActiveCell.Value = "ref_id"
        
        Range(ColTitle & 1).Select
        ActiveCell.Value = "title"
        
        Range(ColLyricist & 1).Select
        ActiveCell.Value = "lyricist"
        
        Range(ColComposer & 1).Select
        ActiveCell.Value = "composer"
        
        Range(ColAuthor & 1).Select
        ActiveCell.Value = "author"
        
        'Range(ColDedicatee & 1).Select
        'ActiveCell.Value = "dedicatee"
        
        Range(ColPublisher & 1).Select
        ActiveCell.Value = "publisher"
        
        Range(ColDate & 1).Select
        ActiveCell.Value = "year"
        
        Range(ColSubject & 1).Select
        ActiveCell.Value = "subject"
        
        Range(ColForm & 1).Select
        ActiveCell.Value = "form"
        
        Range(ColKeySig & 1).Select
        ActiveCell.Value = "key_sig"
        
        Range(ColTimeSig & 1).Select
        ActiveCell.Value = "time_sig"
        
        Range(ColTempo & 1).Select
        ActiveCell.Value = "tempo"
        
        Range(ColInstr & 1).Select
        ActiveCell.Value = "instruments"
        
        'Range(ColCoverDesc & 1).Select
        'ActiveCell.Value = "cover_desc"
        
        Range(ColNote & 1).Select
        ActiveCell.Value = "note"
        
        Range(ColPageCount & 1).Select
        ActiveCell.Value = "page_count"
        
        Range(ColIndexStart & 1).Select
        ActiveCell.Value = "index_start"
    Else
        rowStartIndex = 0
    End If
    
    Range("A1", "P" & sheetCount + 1).Select
    Selection.NumberFormat = "@"
    
    For i = 1 To sheetCount
        Dim colIndex
        colIndex = i + rowStartIndex
        
        'Range(ColRefId & colIndex).Select
        'ActiveCell.FormulaR1C1 = ListRefId(i)
        
        Range(ColTitle & colIndex).Select
        ActiveCell.FormulaR1C1 = ListTitle(i)
        
        Range(ColLyricist & colIndex).Select
        ActiveCell.FormulaR1C1 = ListLyricist(i)
        
        Range(ColComposer & colIndex).Select
        ActiveCell.FormulaR1C1 = ListComposer(i)
        
        Range(ColAuthor & colIndex).Select
        ActiveCell.FormulaR1C1 = ListAuthor(i)
        
        Range(ColPublisher & colIndex).Select
        ActiveCell.FormulaR1C1 = ListPublisher(i)
        
        'Range(ColDedicatee & colIndex).Select
        'ActiveCell.FormulaR1C1 = ListDedicatee(i)
        
        Range(ColDate & colIndex).Select
        ActiveCell.FormulaR1C1 = ListDate(i)
        
        Range(ColSubject & colIndex).Select
        ActiveCell.FormulaR1C1 = ListSubject(i)
        
        Range(ColForm & colIndex).Select
        ActiveCell.FormulaR1C1 = ListForm(i)
        
        Range(ColKeySig & colIndex).Select
        ActiveCell.FormulaR1C1 = ListKeySig(i)
        
        Range(ColTimeSig & colIndex).Select
        ActiveCell.FormulaR1C1 = ListTimeSig(i)
        
        Range(ColTempo & colIndex).Select
        ActiveCell.FormulaR1C1 = ListTempo(i)
        
        Range(ColInstr & colIndex).Select
        ActiveCell.FormulaR1C1 = ListInstr(i)
        
        Range(ColNote & colIndex).Select
        ActiveCell.FormulaR1C1 = ListNote(i)
        
        Range(ColPageCount & colIndex).Select
        ActiveCell.FormulaR1C1 = ListPageCount(i)
        
        Range(ColIndexStart & colIndex).Select
        ActiveCell.FormulaR1C1 = ListIndexStart(i)
    Next
    ExportToCSV (CollectionRefId)
End Sub

Sub ExportToCSV(filename)
    Dim MyPath As String
    Dim MyFileName As String
    'The path and file names:
    MyPath = ActiveWorkbook.Path
    MyFileName = filename & "_Reformatted_" & Format(Date, "yyyy-mm-dd")
    'Makes sure the path name ends with "\":
    If Not Right(MyPath, 1) = "\" Then MyPath = MyPath & "\"
    'Makes sure the filename ends with ".csv"
    If Not Right(MyFileName, 4) = ".xlsx" Then MyFileName = MyFileName & ".xlsx"
    'Copies the sheet to a new workbook:
    Sheets("Data").Copy
    'The new workbook becomes Activeworkbook:
    With ActiveWorkbook
    'Saves the new workbook to given folder / filename:
        .SaveAs filename:= _
            MyPath & MyFileName, _
            FileFormat:=xlOpenXMLWorkbook, _
            CreateBackup:=False
    'Closes the file
        .Close False
    End With
End Sub
