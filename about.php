<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="romantic_era_lyrics">

<head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<title>Romantic Era Lyrics</title>
	<!-- <link type="text/css" rel="stylesheet" href="style.css" /> -->
	<meta name="description" content="Romantic-Era Lyrics Score Page" />

	<!-- Typekit -->
	<script src="//use.typekit.net/izf8zmo.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

    <!-- Angular JS -->
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-route.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-sanitize.js"></script>
	<script src="js/plugins/dirPagination.js"></script>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/mystyle.css" rel="stylesheet">
    <link href="css/scrollbar.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <!-- Romantic-Era Lyrics Application File -->
	<script src="appjs/real.js"></script>
</head>

<body>
	<div ng-include="'./templates/header.html'">
	</div>

	<div class="container container-main" ng-cloak>
		<div class="row">
			<div class="col-md-12">
				<h1>About Romantic-Era Lyrics</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-offset-1 col-md-10 about-text">
				<span id='intro' class='editable'>
				<?php 
				$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
				include( "$phpAbs/templates/about/introduction.txt" );
				?>
				</span>
				<h2>Scope and Arrangement</h2>
				<div ng-controller="info_controller as info" ng-cloak>
					<p>The database contains <b>{{ genInfo.score_count }} scores</b>, including <b>{{ genInfo.collection_count }} collections</b>, with <b>{{ genInfo.composer_count }} composers</b>, <b>{{ genInfo.lyricist_count }} poets</b>, and <b>{{ genInfo.publisher_count }} publishers</b> on record.</p>
					<p>We have adopted some specific terminology to organize and distinguish the scores in our database:</p>
					<p>A <b>score</b> is an individual piece of sheet music. A score has a <b>poet</b>, who wrote the lyrics on the sheet music, and a <b>composer</b>, who wrote the music itself. These are sometimes both the same person. Some poets, such as Caroline Norton, were composers as well.</p>
					<p>We distinguish between a <b>work</b> of literature versus an individual score. Each individual score corresponds to a work of literature, and multiple scores may be based on the same work. For example, we have multiple scores representing the work of literature <i>Auld Robin Gray</i>.</p>
					<p>A <b>collection</b> is simply some work of literature that contains multiple scores, such as a book of sheet music.</p>
				</div>
				<h2>Advanced Searching</h2>
				<p>The <a href="./search.html">search page</a> allows users to search for items in each of the categories outlined above: score, work, artist, publisher, and collection. Searching is based on a different set of criteria for each item, such as by title, poet, lyricist, or publisher for musical scores. All of these criteria are selected by default, but can be deselected if more specific search functionality is desired.</p>
				<p>The search page selects records from our data by looking for records for which at least one of the selected search criteria match for each separate search term the user submits. For example, if a user submits the search terms "love norton" for scores in all criteria, scores or "I Do Not Love Thee" and "Love Not" by Caroline Norton will be displayed because "love" is found in the title, and "norton" matches the composer or poet criteria. Both "love" and "norton" must be found in at least one of the criteria.</p>
				<p>In addition to search criteria, there are additional options to limit search results for several of the categories. For example, the user can limit the search to scores for which performances are available.</p>
				<h2>Technical Design</h2>
				<p>The data for the individual scores, originally written in the Dublin Core standard, was imported into a MySQL database. From this data information about publishers, composers, and poets was extrapolated.</p>
				<p>This separate follows the model-view-controller concept, meaning the data about the webpages through which the data is viewed are separate in development. This allows users who wish to access the raw data about any scores to do so through the website API. This also allows us to make simple aesthetic changes to the website without disrupting functionality.</p>
				<p>The backend of this website was built on top of a MySQL database and an API written in PHP.</p>
				<p>The frontend was developed using <a href="https://angularjs.org/" target="new">AngularJS</a>, a front-end javascript framework that allows for templating of the data from the database.</p>
				<h2>Accessing the Romantic-Era Lyrics API</h2>
				<p>The API structure matches the structure of the HTML pages.</p>
				<p>The API consists of PHP files of the same name as the HTML page.</p>
				<p>For example, to access the appropriate data for a specific score page:</p>
				<p>The page url:</p>
				<code><a href="./score.html#?id=40">.../score.html#?id=40</a></code>
				<p>The api url:</p>
				<code><a href="./api/score.php?id=40">.../api/score.php?id=40</a></code>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h1>Acknowledgements</h1>
			</div>
		</div>
		<div class="row about-text">
			<div class="col-md-10 col-md-offset-1">
				<p>Major support for this project was provided by the Office of the Provost’s <a href="http://www.sc.edu/about/offices_and_divisions/provost/faculty/grants/internalgrants/index.php" target="new">Internal Grant Programs</a> at the University of South Carolina. Additional funding was obtained through the <a href="http://www.sc.edu/our/magellan.shtml" target="new">Magellan Scholar program</a> for undergraduate research.</p>

				<p>Further assistance was provided by the <a href="http://sc.edu/about/centers/digital_humanities/index.php" target="new">Center for Digital Humanities</a> and the University of <a href="http://library.sc.edu/p/TCL" target="new">South Carolina Libraries</a>, and much of the displayed scores came from the private collection of Paula Feldman.</p>
				<br>
				<h2 class="ack-title">Principal Investigator</h2>
				<p>Paula R. Feldman</p>
				<br>
				<h2 class="ack-title">Developer</h2>
				<p>Victor Reynolds</p>
				<p>Matt Short</p>
				<br>
				<h2 class="ack-title">Design</h2>
				<p>Aidan Zanders</p>
				<p>Victor Reynolds</p>
				<br>
				<h2 class="ack-title">Augmented Notes</h2>
				<p>Courtesy of <a href="https://annieswafford.wordpress.com/" target="new">Joanna Swafford</a></p>
				<br>
				<h2 class="ack-title">Data Entry</h2>
				<span id='DataEntryContributors' class='editable'>
				<?php 
				$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
				include( "$phpAbs/templates/about/data-entry-contributors.txt" );
				?>
				</span>
				<br>

				<h2>Technical Acknowledgements</h2>
				<p>We would like to thank <a href="https://annieswafford.wordpress.com/" target="new">Joanna Swafford</a> for allowing us to use <a href="http://www.augmentednotes.com/" target="new">Augmented Notes</a> to provide the visualizations for our recorded pieces.</p>
				<p>The frontend of this website was built using <a href="https://angularjs.org" target="new">AngularJS</a> and <a href="http://getbootstrap.com/" target="new">Bootstrap</a>.</p>
				<p>The backend of this website was built with <a href="https://www.mysql.com/" target="new">MySQL</a> and <a href="https://www.php.net/" target="new">PHP</a>.</p>
				<p>We would also like to thank the creator of the <a href="http://glyphicons.com/" target="new">Glyphicon</a> Halflings set for allowing us to use the icon set through Bootstrap.</p>
				<p>In addition, we thank <a href="http://e-smartdev.com" target="new">Damien Corzani</a> for the <a href="https://github.com/e-smartdev/smartJQueryZoom" target="new">smartJQueryZoom plugin</a> and <a href="http://www.michaelbromley.co.uk/" target="new">Michael Bromley</a> for the <a href="https://github.com/michaelbromley/angularUtils/tree/master/src/directives/pagination" target="new">AngularJS Pagination directive</a>.</p>

			</div>
		</div>
	</div>

	<div ng-include="'./templates/footer.html'">
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/script.js"></script>
	
</body>

</html>