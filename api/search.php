<?php
	/* search.php
	 * Author: Victor Reynolds
	 *
	 * The search API component, and the most complicated API piece due to the amount of accepted user input.
	 * Four variables are submitted via the querystring to this api:
	 *  searchin - What data category we search in, e.g. scores, publishers, artists, works, or collections
	 *  searchtype - What fields we search by, separated by commas, e.g. title for scores, or first and last name for artists, etc.
	 *  searchstring - the string of data that is compared to the fields selected in the searchtype variable
	 *  caveat - Predefined SQL clauses selected separated by comma. They are defined in the $caveats_list variable below.
	 *
	 * The search works as follows. These stages are indicated in the code.
	 * Stage 1: a predefined query is selected based on the provided searchin variable
	 * Stage 2: (skip if searchstring undefined) the searchstring is split by spaces and clauses are appended to the base query
	 *          so that for each separate word in the searchstring, that word must match
	 *          at least one of the indicated fields selected in searchtype. For example if we search:
	 *          "love norton" as our searchstring in scores (as our searchin value) and by title,composer as our searchtype,
	 *          The following will be added to our query that finds information for all scores:
	 *          "WHERE (title LIKE '%love%' OR composer LIKE '%love%') AND (title LIKE '%norton%' OR composer LIKE '%norton%')"
	 *          
	 *          "norton" will match for composer and "love" will match for title for the scores "Love Not" and "I Do Not Love Thee"
	 * Stage 3: if caveats are defined, they are selected and appended to the where clause from the predefined $caveats_list variable
	 * Stage 4: the query is submitted
	 * Stage 5: results are formatted uniformally so that we can use the same logic to display all types of results
	 *          in the search and browse pages. An id, thumbnail, title value, and data values (e.g. composer, location, etc.) are
	 *          defined from the resulting query for each returned item.
	 * Stage 6: this file responds with a json array of the formatted data
	 *
	 * Here's an example querystring:
	 * search.php?searchstring=auld+brysson&searchtype=title%2Ccomposer%2Clyricist%2Cpublisher&searchin=score&caveat=has_augnotes
	 *
	 * We have "auld brysson" as the searchstring
	 * title, composer, lyricist, and publisher for the searchtype
	 * score as our searchin category
	 * has_augnotes as our caveat
	*/

	require_once("lib/API_Utilities.php");

	// Fields that we can search by that are included to compare the user-provided searchstring against.
	$searches = array(
				"score"=>array("title", "composer", "lyricist", "publisher"),
				"artist"=>array("f_name", "l_name", "biography"),
				"publisher"=>array("name", "city", "description"),
				"collection"=>array("title", "publisher", "score_titles"),
				"work"=>array("title", "lyricist", "publisher_list", "composer_list")
				);

	// Boolean caveats that can be appended to searches
	$caveats_list = array(
				"score"=>array(
							"has_augnotes"=>"augnotes_count > 0"
						),
				"artist"=>array(
							"is_composer"=>"score_count > 0",
							"is_lyricist"=>"work_count > 0",
							"has_biography"=>"biography IS NOT NULL"
						),
				"work"=>array(
							"multiple_scores"=>"score_count > 1"
						),
				"publisher"=>array(
							"has_description"=>"description IS NOT NULL"
						)
				);

	// This function generates the appropriate caveat list to append to the end of
	// our SQL query. It simply selects the caveats based on the provided 
	// caveat and searchin values through the URL and appends them together with " AND "
	// if necessary.
	function createCaveatQuery()
	{
		global $caveats_list;

		if (isset($_GET["caveat"]) && isset($_GET["searchin"]))
		{
			$caveats_param = $_GET["caveat"];
			$searchin = $_GET["searchin"];

			$array_caveats = array();

			$array_params_caveats = explode(",", $caveats_param);

			foreach($array_params_caveats as $caveat_param_item)
			{
				if (isset($caveats_list[$searchin][$caveat_param_item]))
				{
					array_push($array_caveats, $caveats_list[$searchin][$caveat_param_item]);
				}
			}

			if (count($array_caveats) > 0)
			{
				return implode($array_caveats, " AND ");
			}

			return "";
		}
	}

	if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["searchin"])) {

		$searchin = $_GET["searchin"];
		$returnArray = array();

        /* Stage 1: a predefined query is selected based on the provided searchin variable
         * Try copying and pasting one of these into phpmyadmin and to an online SQL formatter
         * to get a better idea of what is going on if necessary.
        */
		if ($searchin == "score")
		{
			$sql_search = 'SELECT * FROM (SELECT Score.id as id, Score.title as title, (SELECT GROUP_CONCAT(CONCAT(IFNULL(composers.f_name, " "), " ", composers.l_name) SEPARATOR "; ") FROM Artist as composers INNER JOIN Artist_has_Score ON composers.id=Artist_has_Score.Artist_id WHERE Artist_has_Score.Score_id=Score.id GROUP BY Artist_has_Score.Score_id) as composer, (SELECT GROUP_CONCAT(CONCAT(IFNULL(lyricists.f_name, " "), " ", lyricists.l_name) SEPARATOR "; ") FROM Artist as lyricists INNER JOIN Artist_has_Work ON lyricists.id=Artist_has_Work.Artist_id LEFT OUTER JOIN Score as work_score ON work_score.Work_id = Artist_has_Work.Work_id WHERE work_score.id=Score.id GROUP BY work_score.id) as lyricist, (SELECT GROUP_CONCAT(DISTINCT Publisher.name SEPARATOR "; ") FROM Publisher INNER JOIN Publisher_has_Score ON Publisher_has_Score.Publisher_id=Publisher.id WHERE Publisher_has_Score.Score_id=Score.id GROUP BY Publisher_has_Score.Score_id) as publisher, (SELECT COUNT(*) FROM Augnotes WHERE Augnotes.Score_id=Score.id) as augnotes_count FROM Score ORDER BY title ASC) as result';
		}
		elseif ($searchin == "artist")
		{
			$sql_search = 'SELECT * FROM (SELECT *, (SELECT COUNT(*) FROM Artist_has_Score WHERE Artist_has_Score.Artist_id = Artist.id) as score_count, ' .
			              '(SELECT COUNT(*) FROM Artist_has_Work RIGHT OUTER JOIN Score On Artist_has_Work.Work_id = Score.Work_id WHERE ' .
			              'Artist_has_Work.Artist_id = Artist.id) as work_count FROM Artist ORDER BY l_name ASC) as result';
		}
		elseif ($searchin == "publisher")
		{
			$sql_search = 'SELECT * FROM (SELECT id, name, city, description, (SELECT COUNT(*) FROM Publisher_has_Score WHERE Publisher_has_Score.Publisher_id = Publisher.id) as score_count FROM Publisher ORDER BY name ASC) as result';
		}
		elseif ($searchin == "collection")
		{
			// TODO: use Group_Concat() to group scores
			$sql_search = 'SELECT * FROM (SELECT Collection.ref_id as ref_id, Collection.title as title,Collection.id as id,Publisher.name as publisher, (SELECT GROUP_CONCAT(Score.title SEPARATOR ", ") FROM Score INNER JOIN Collection_has_Score as Col_Ref ON Score.id = Col_Ref.Score_id WHERE Col_Ref.Collection_id = Collection.id GROUP BY Col_Ref.Collection_id) as score_titles FROM Collection INNER JOIN Publisher_has_Collection ON Publisher_has_Collection.Collection_id = Collection.id INNER JOIN Publisher ON Publisher.id = Publisher_has_Collection.Publisher_id ORDER BY title ASC) as result';
		}
		elseif ($searchin == "work")
		{
			$sql_search = 'SELECT * FROM (SELECT Work.id, Work.title, (SELECT id FROM Score WHERE Score.work_id = Work.id LIMIT 1) as first_score_id, (SELECT COUNT(*) FROM Score WHERE Score.Work_id = Work.id) as score_count, (SELECT GROUP_CONCAT(CONCAT(IFNULL(composer.f_name, " "), " ", composer.l_name) SEPARATOR "; ") FROM Artist as composer INNER JOIN Artist_has_Score ON Artist_has_Score.Artist_id = composer.id INNER JOIN Score on Score.id = Score_id WHERE Score.Work_id = Work.id) as composer_list, (SELECT GROUP_CONCAT(CONCAT(Publisher.city, ": ", Publisher.name) SEPARATOR "; ") FROM Publisher INNER JOIN Publisher_has_Score ON Publisher.id = Publisher_has_Score.Publisher_id INNER JOIN Score ON Score.id = Publisher_has_Score.Score_id WHERE Score.Work_id = Work.id) as publisher_list, (SELECT GROUP_CONCAT(CONCAT(IFNULL(lyricist.f_name, " "), " ", lyricist.l_name) SEPARATOR "; ") FROM Artist as lyricist INNER JOIN Artist_has_Work ON Artist_has_Work.Artist_id = lyricist.id WHERE Artist_has_Work.Work_id = Work.id) as lyricist FROM Work ORDER BY Work.title) as result';
		}

		 /* Stage 2: (skip if searchstring undefined) the searchstring is split by spaces and clauses are appended to the base query
		 *          so that for each separate word in the searchstring, that word must match
		 *          at least one of the indicated fields selected in searchtype. For example if we search:
		 *          "love norton" as our searchstring in scores (as our searchin value) and by title,composer as our searchtype,
		 *          The following will be added to our query that finds information for all scores:
		 *          "WHERE (title LIKE '%love%' OR composer LIKE '%love%') AND (title LIKE '%norton%' OR composer LIKE '%norton%')"
		 *          
		 *          "norton" will match for composer and "love" will match for title for the scores "Love Not" and "I Do Not Love Thee"
		 */
		if (isset($_GET["searchtype"]) && isset($_GET["searchstring"]))
		{
			$searchtype = $_GET["searchtype"];

			$searchtype = explode(",", $searchtype);
			$searchfields = array();
			foreach($searchtype as $searchtype_value)
			{
				if (in_array($searchtype_value, $searches[$searchin]))
				{
					array_push($searchfields, $searchtype_value);
				}
				else
				{
					echo "Invalid search type: \"" . $searchtype_value . "\"";
					exit();
				}
			}

			$searchstring = $_GET["searchstring"];
			
			// Construct what we'll append to the query based on the provided searchtype options
			// Split search string into separate words by spaces
			// Construct query so that each word in the search string must match at least one searchtype option
			$searchvalues = explode(" ", $searchstring);
			$sql_param_search = array();
			$sql_search_append = array();
			foreach($searchvalues as $index=>$word)
			{
				$marker = ":searchstring" . $index;
				$sql_param_search[$marker] = $word;
				$or_clauses = array();

				foreach($searchfields as $field)
				{
					array_push($or_clauses, $field . ' LIKE CONCAT("%", ' . $marker . ', "%")');
				}

				array_push($sql_search_append, implode($or_clauses, " OR "));
			}

			$sql_search_append = "( " . implode($sql_search_append, " ) AND ( ") . " )";

			$sql_search = $sql_search . " WHERE " . $sql_search_append;

            // Stage 3: if caveats are defined, they are selected and appended 
            // to the where clause from the predefined $caveats_list variable
			$caveats_str = createCaveatQuery();
			if ($caveats_str != "")
			{
				$sql_search = $sql_search . " AND " . $caveats_str;
			}

			// Stage 4: the query is submitted
			$result = fetchData($sql_search, $sql_param_search);
		}
		else
		{
            // Stage 3: if caveats are defined, they are selected and appended 
            // to the where clause from the predefined $caveats_list variable
            // Note: we still want to do this if a searchstring is not defined
            // E.g. Browse by Performance - we want to see all of the scores
            // for which augmented notes pieces are available.
			$caveats_str = createCaveatQuery();
			if ($caveats_str != "")
			{
				$sql_search = $sql_search . " WHERE " . $caveats_str;
			}

			// Stage 4: the query is submitted
			$result = fetchData($sql_search);
		}

		/* Stage 5: results are formatted uniformally so that we can use the same logic to display all types of results
		 *          in the search and browse pages. An id, thumbnail, title value, and data values (e.g. composer, location, etc.) are
		 *          defined from the resulting query for each returned item.
		*/
		if ($searchin == "score")
		{
			foreach ($result as $index=>$item)
			{
				$returnItem = array();

				$returnItem["title"] = $item["title"];

				$returnItem["id"] = $item["id"];

				$imageArray = findImagesFromScoreId($item["id"]);

				if (!empty($imageArray) && !empty($imageArray["thumbs"]))
				{
					$returnItem["image"] = $imageArray["thumbs"][0];
				}

				$returnItem["data"] = array();

				$returnItem["data"]["Composer"] = $item["composer"];
				$returnItem["data"]["Poet"] = $item["lyricist"];
				$returnItem["data"]["Publisher"] = $item["publisher"];

				$returnArray[$index] = $returnItem;
			}
		}
		elseif ($searchin == "artist")
		{
			foreach ($result as $index=>$item)
			{
				$returnItem = array();

				$returnItem["title"] = $item["f_name"] . " " . $item["l_name"];

				$returnItem["id"] = $item["id"];

				$returnItem["image"] = find_artist_thumbnail($item["id"], $item["score_count"], $item["work_count"]);

				$returnItem["data"] = array();

				$returnItem["data"]["Composed scores"] = $item["score_count"];
				$returnItem["data"]["Written lyrics"] = $item["work_count"];

				$bio_snippet = $item["biography"];
				if ($bio_snippet)
				{
					$bio_snippet = substr($bio_snippet, 0, 150);
					// Replace HTML entities with empty space, add ... to end of bio
					$bio_snippet = preg_replace('/<.*?>/', '', $bio_snippet) . "...";
					$returnItem["data"]["Biography"] = $bio_snippet;
				}

				$returnArray[$index] = $returnItem;
			}
		}
		elseif ($searchin == "publisher")
		{
			foreach ($result as $index=>$item)
			{
				$returnItem = array();

				$returnItem["title"] = $item["name"];

				$returnItem["id"] = $item["id"];

				$returnItem["image"] = find_publisher_thumbnail($item["id"]);

				$returnItem["data"] = array();

				$returnItem["data"]["Location"] = $item["city"];
				$returnItem["data"]["Scores Available"] = $item["score_count"];

				$bio_snippet = $item["description"];
				if ($bio_snippet)
				{
					$bio_snippet = substr($bio_snippet, 0, 150);
					// Replace HTML entities with empty space, add ... to end of bio
					$bio_snippet = preg_replace('/<.*?>/', '', $bio_snippet) . "...";
					$returnItem["data"]["Description"] = $bio_snippet;
				}

				$returnArray[$index] = $returnItem;
			}
		}
		elseif ($searchin == "collection")
		{
			foreach ($result as $index=>$item)
			{
				$returnItem = array();

				$returnItem["title"] = $item["title"];

				$returnItem["id"] = $item["id"];

				$returnItem["image"] = find_collection_thumbnail($item["ref_id"]);

				$returnItem["data"] = array();

				$returnItem["data"]["Publisher"] = $item["publisher"];

				$returnArray[$index] = $returnItem;
			}
		}
		elseif ($searchin == "work")
		{
			foreach ($result as $index=>$item)
			{
				$returnItem = array();

				$returnItem["title"] = $item["title"];
				$returnItem["id"] = $item["id"];

				$imageArray = findImagesFromScoreId($item["first_score_id"]);

				$returnItem["image"] = $imageArray["thumbs"][0];

				$returnItem["data"] = array();
				$returnItem["data"]["Poet"] = $item["lyricist"];
				$returnItem["data"]["Available Scores"] = $item["score_count"];

				$returnItem["extra_data"] = array();
				$returnItem["extra_data"]["first_score_id"] = $item["first_score_id"];

				$returnArray[$index] = $returnItem;
			}
		}

        // Stage 6: this file responds with a json array of the formatted data
		header("Content-Type: application/json");
		echo(json_encode($returnArray));
	}
?>
