<?php
	$bio_text_path = "./bio_texts/";

	require_once("../conf/config.php");

	function addArtistBiography($bio_data)
	{
		global $db;

		$find_sql = "SELECT * FROM artist WHERE l_name LIKE CONCAT('%', :l_name, '%')";
		$params_find_sql = array("l_name"=>$bio_data["l_name"]);

		$return = $db->prepare($find_sql);
		$return->execute($params_find_sql);
		$return = $return->fetchAll(PDO::FETCH_ASSOC);

		if (count($return) > 1)
		{
			echo("Multiple results found. Narrowing search for " . $bio_data["f_name"] . " " . $bio_data["l_name"] . " to last name and first initial.\n");

			$find_sql = "SELECT * FROM artist WHERE f_name LIKE CONCAT('%', :f_letter, '%') AND l_name LIKE CONCAT('%', :l_name, '%')";
			$params_find_sql = array("l_name"=>$bio_data["l_name"], "f_letter"=>substr($bio_data["f_name"], 0, 1));

			$return = $db->prepare($find_sql);
			$return->execute($params_find_sql);
			$return = $return->fetchAll(PDO::FETCH_ASSOC);

			if (count($return) > 1)
			{
				echo("Multiple results still found. Narrowing search for " . $bio_data["f_name"] . " " . $bio_data["l_name"] . " to last name and full first name.\n");

				$find_sql = "SELECT * FROM artist WHERE f_name LIKE CONCAT('%', :f_name, '%') AND l_name LIKE CONCAT('%', :l_name, '%')";
				$params_find_sql = array("l_name"=>$bio_data["l_name"], "f_name"=>$bio_data["f_name"]);

				$return = $db->prepare($find_sql);
				$return->execute($params_find_sql);
				$return = $return->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		if (count($return) == 1)
		{
			$bio_sources = implode($bio_data["sources"], "~");
			$update_sql = "UPDATE artist SET born=:born, died=:died, biography=:bio_text, m_name=:m_name, biography_sources=:bio_sources WHERE id=:id";
			$params_update_sql = array("born"=>$bio_data["born"], "died"=>$bio_data["died"], "m_name"=>$bio_data["m_name"], "bio_text"=>$bio_data["bio_text"], "id"=>$return[0]["id"], "bio_sources"=>$bio_sources);

			$update = $db->prepare($update_sql);
			$update->execute($params_update_sql);
			echo("Successfully imported biography for " . $bio_data["f_name"] . " " . $bio_data["l_name"] . "\n\n");
		}
		elseif (count($return) == 0)
		{
			echo("Was not able to find artist name " . $bio_data["f_name"] . " " . $bio_data["l_name"] . "\n\n");
		}
		elseif (count($return) > 1)
		{
			echo("Duplicate entries exist for " . $bio_data["f_name"] . " " . $bio_data["l_name"] . "\n\n");
		}
	}

	$bio_files = scandir($bio_text_path);

	foreach ($bio_files as $index=>$file)
	{
		if ($index > 1)
		{
			$import_file = file_get_contents($bio_text_path . $file);

			addArtistBiography(json_decode($import_file, true));
		}
	}
?>