<?php


/*====================================================================

    DOCUMENT HEADER

====================================================================*/

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "conf/database.php";
include_once $phpAbs . "admin/class.user.php";
include_once $phpAbs . "admin/editor/editorFunctions.php";

$user = new user( $db );


/*====================================================================
    Basic User Redirects
====================================================================*/


/*--------------------------------------------------------------------
    Redirect all users that aren't logged in to the login page
--------------------------------------------------------------------*/

if( !$user->is_logged_in() && !isset($onLoginPage) )
{
    $user->redirect( $htmlAbs . "admin/index.php" );
}


/*--------------------------------------------------------------------
    Redirect logged in users from the login page to the dashboard
--------------------------------------------------------------------*/

else if( $user->is_logged_in() && isset($onLoginPage) )
{
    $user->redirect( $htmlAbs . "admin/dashboard.php" );
}

?>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1" />

<link rel="stylesheet" type="text/css" href="<?= $htmlAbs . "admin/style.css" ?>">

<title>Dashboard</title>
</head>

<body>

<?php if( $user->is_logged_in() ){ ?>
<div id="nav-primary" class="navigation">

    <div class='nav-left'>
        <a href="<?= $htmlAbs . "admin/dashboard.php" ?>">Dashboard</a>
    </div>

    <div class='nav-right'>
        <a href="<?= $htmlAbs . "admin/documentation/help.php" ?>">Documentation</a>
        <a href="<?= $htmlAbs . "admin/account.php" ?>">Account</a>
        <a href="<?= $htmlAbs . "admin/logout.php" ?>">Log Out</a>
    </div>

</div>
<?php } ?>