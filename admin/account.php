<?php
$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "admin/templates/header.php";

if( isset( $_POST['newEmail'] ) )
{
    $dbStatement = $db->prepare( "SELECT * FROM users WHERE userEmail=:userEmail LIMIT 1" );

    $dbStatement->bindParam( ':userEmail', $_POST['currentEmail'] );
    $dbStatement->execute();

    $userRow = $dbStatement->fetch(PDO::FETCH_ASSOC);

    if( $dbStatement->rowCount() > 0 && password_verify( $_POST['currentPassword'], $userRow['userPass'] ) )
    {
        $updateStatement = $db->prepare( "UPDATE users SET userEmail=:newEmail WHERE userEmail=:currentEmail" );

        $updateStatement->bindParam( ':newEmail', $_POST['newEmail'] );
        $updateStatement->bindParam( ':currentEmail', $_POST['currentEmail'] );
        $updateStatement->execute();
    }
}

else if ( isset( $_POST['newPassword'] ) && isset( $_POST['newPasswordConfirm'] ) )
{
    $dbStatement = $db->prepare( "SELECT * FROM users WHERE userEmail=:userEmail LIMIT 1" );

    $dbStatement->bindParam( ':userEmail', $_POST['currentEmail'] );
    $dbStatement->execute();

    $userRow = $dbStatement->fetch(PDO::FETCH_ASSOC);

    if( $dbStatement->rowCount() > 0 && password_verify( $_POST['currentPassword'], $userRow['userPass'] ) )
    {
        $updateStatement = $db->prepare( "UPDATE users SET userPass=:newPass WHERE userEmail=:currentEmail" );

        $updateStatement->bindParam( ':newPass', $_POST['newPassword'] );
        $updateStatement->bindParam( ':currentEmail', $_POST['currentEmail'] );
        $updateStatement->execute();
    }
}
?>

<form id='account' method="post">

    <h2>Update Email Address</h2>

    <h4>Email</h4>

    <input type="text" name="newEmail" placeholder='New Email'/>


    <h2>Update Password</h2>

    <input type="text" id='password' name="newPassword" placeholder='New Password' />
    <input type="text" id='passwordConfirm' name="newPasswordConfirm" placeholder='Confirm New Password'/>


    <h2>Update Security Question</h2>

    <input type='text' name='newSecurityQuestion' placeholder='New Security Question'>
    <input type='text' name='newSecurityAnswer' placeholder='Answer'>

    <hr>


    <h2>Verify Yourself</h2>

    <input type='text' name='currentEmail' placeholder='Current Email Address' required />
    <input type="text" name="currentPassword" placeholder='Current Password' required />

    <button type="submit" class='primary'>Submit</button>
    
    <div class='alert success' style='display: none'>
        <span class='closebtn' onclick="this.parentElement.style.display='none';">&times;</span>
        Success!
    </div>
    <div class='alert failure' style='display: none'>
        <span class='closebtn' onclick="this.parentElement.style.display='none';">&times;</span>
        Error posting data.
    </div>

</form>

<?php
include_once $phpAbs . "admin/templates/footer.php";
?>