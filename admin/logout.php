<?php

include_once '../conf/database.php';
include_once 'class.user.php';

$user = new user( $db );
$user->logout();

if( !$user->is_logged_in() )
{
    $user->redirect('index.php');
}

?>