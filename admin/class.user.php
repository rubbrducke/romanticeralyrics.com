<?php

class user 
{
    private $db;


    /*====================================================================

        CONSTRUCT

    ====================================================================*/

    function __construct($db_connect)
    {
        $this->db = $db_connect;
    }


    /*====================================================================

        IS LOGGED IN

    ====================================================================*/

    public function is_logged_in()
    {
        if( isset($_SESSION['user_session']) )
        {
            if( $_SESSION['timer'] -= 1 == 0 )
            {
                session_regenerate_id(true);
                $_SESSION['timer'] = 5;
            }
            return true;
        }

        return false;
    }


    public function isAdmin()
    {
        if( isset( $_SESSION['user_session'] ) && $_SESSION['admin'] == '1' )
        {
            return true;
        }

        return false;
    }

    public function canCreate()
    {
        if( isset( $_SESSION['user_session'] ) && $_SESSION['create'] == '1' )
        {
            return true;
        }

        return false;
    }

    public function canModify()
    {
        if( isset( $_SESSION['user_session'] ) && $_SESSION['modify'] == '1' )
        {
            return true;
        }

        return false;
    }

    public function canUpload()
    {
        if( isset( $_SESSION['user_session'] ) && $_SESSION['upload'] == '1' )
        {
            return true;
        }

        return false;
    }

    public function canDelete()
    {
        if( isset( $_SESSION['user_session'] ) && $_SESSION['delete'] == '1' )
        {
            return true;
        }

        return false;
    }


    /*====================================================================

        REDIRECT

    ====================================================================*/

    public function redirect($url)
    {
        header("Location: $url");
    }


    /*====================================================================
        
        LOGOUT 

    ====================================================================*/

    public function logout()
    {
        session_destroy();
        unset($_SESSION['user_session']);
        return true;
    }


    /*====================================================================

        LOGIN 

    ====================================================================*/

    public function login( $userEmail, $userPassword, $userIP )
    {
        try
        {
            $dbStatement = $this->db->prepare("SELECT * FROM Users WHERE userEmail=:userEmail LIMIT 1");
            $dbStatement->bindParam( ':userEmail', $userEmail );
            $dbStatement->execute();

            $userRow = $dbStatement->fetch(PDO::FETCH_ASSOC);

            if( $dbStatement->rowCount() > 0 && password_verify( $userPassword, $userRow['userPass'] ) )
            {
                $_SESSION['user_session'] = $userRow['userID'];
                $_SESSION['timer'] = 5;

                $_SESSION['admin'] = $userRow['admin'];
                $_SESSION['create'] = $userRow['creation'];
                $_SESSION['modify'] = $userRow['modification'];
                $_SESSION['delete'] = $userRow['deletion'];
                $_SESSION['upload'] = $userRow['fileUpload'];

                return true;
            } // if

            return false;

        } // try

        catch( PODException $e )
        {
            echo 'Error connecting to server.';
            return false;
        }
    } // function


    /*====================================================================

        CHECK KNOWN IP

    ====================================================================*/

    public function checkKnownIP( $userEmail, $userIP )
    {
        $dbStatement = $this->db->prepare( "SELECT * FROM User_IPs WHERE userEmail=:userEmail AND userIP=:userIP LIMIT 1" );
        $dbStatement->bindParam( ':userEmail', $userEmail );
        $dbStatement->bindParam( ':userIP', $userIP );

        $dbStatement->execute();

        $match = $dbStatement->rowCount();
        $userRow = $dbStatement->fetch(PDO::FETCH_ASSOC);

        if( $match > 0 && $userRow['known'] == 1 )
        {
            return true;
        } // if

        else if ( ! $match > 0 )
        {
            $dbStatement = $this->db->prepare( "INSERT INTO User_IPs (userEmail, userIP, known) VALUES (:userEmail, :userIP, 0)" );
            $dbStatement->bindParam( ':userEmail', $userEmail );
            $dbStatement->bindParam( ':userIP', $userIP );

            $dbStatement->execute();
            return false;
        } // else

    } // function


    /*====================================================================

        CHECK SECURITY QUESTION

    ====================================================================*/

    public function checkSecurityQuestion( $userEmail, $userIP, $question, $answer )
    {
        $dbStatement = $this->db->prepare( "SELECT secAnswer FROM Users WHERE userEmail=:userEmail LIMIT 1" );
        $dbStatement->bindParam( ':userEmail', $userEmail );

        $dbStatement->execute();

        $userRow = $dbStatement->fetch(PDO::FETCH_ASSOC);

        if( $dbStatement->rowCount() > 0 )
        {    
            if( password_verify( strtolower( $answer ), $userRow['secAnswer'] ) )
            {
                $this->validateIP( $userEmail, $userIP );
                return true;
            }
        }

        return false;
    }


    public function validateIP( $userEmail, $userIP )
    {
        $dbStatement = $this->db->prepare( "UPDATE User_IPs SET known=1 WHERE userEmail=:userEmail AND userIP=:userIP" );
        $dbStatement->bindParam( ':userEmail', $userEmail );
        $dbStatement->bindParam( ':userIP', $userIP );

        $dbStatement->execute();
    }


    /*====================================================================

        GET SECURITY QUESTION

    ====================================================================*/
    
    public function getSecurityQuestion( $userEmail )
    {
        $dbStatement = $this->db->prepare( "SELECT secQuestion FROM Users WHERE userEmail=:userEmail LIMIT 1" );
        $dbStatement->bindParam( ':userEmail', $userEmail );

        $dbStatement->execute();

        if( ! $dbStatement->rowCount() > 0 )
        {
            return 'Error. You are not a user!';
        }

        $userRow = $dbStatement->fetch(PDO::FETCH_ASSOC);

        if( $userRow['secQuestion'] != '' )
        {
            return $userRow['secQuestion'];
        }

        else
        {
            return 'Error. You do not have any security questions set. How did this even happen?';
        }
    }


    /*====================================================================

        ATTEMPT AUTHORIZATION

    ====================================================================*/

    public function updateSecurityQuestion( $userEmail, $question, $answer )
    {
            $dbStatement = $this->db->prepare( "UPDATE User_IPs SET question=:question AND answer=:answer WHERE userEmail=:userEmail" );

            $answer = password_hash( $answer, PASSWORD_DEFAULT );
            
            $dbStatement->bindParam( ':userEmail', $userEmail );
            $dbStatement->bindParam( ':question', $question );
            $dbStatement->bindParam( ':answer', $answer );

            $dbStatement->execute();

    } // function

    /*====================================================================

        CHANGE EMAIL

    ====================================================================*/

    public function changeEmail( $newEmail, $oldEmail, $password )
    {

        try
        {
            // Double check their login credentials

            if( $this->login($oldEmail, $password) )
            {
                // Update Users table

                $dbStatement = $this->db->prepare("UPDATE Users SET userEmail=:userEmailNew WHERE userEmail=:userEmailOld");
                $dbStatement->bindParam( ':userEmailNew', $newEmail );
                $dbStatement->bindParam( ':userEmailOld', $oldEmail );

                $dbStatement->execute();

                // Update User IP-Addresses table

                $dbStatement = $this->db->prepare("UPDATE User_IPs SET userEmail=:userEmailNew WHERE userEmail=:userEmailOld");
                $dbStatement->bindParam( ':userEmailNew', $newEmail );
                $dbStatement->bindParam( ':userEmailOld', $oldEmail );

                $dbStatement->execute();

                return true;
            } // if

            else
            {
                return false;
            } // else

        } // try

        catch( PDOException $e )
        {
            echo 'Error connecting to server.';
            return false;
        }

    } // function

} // class

?>