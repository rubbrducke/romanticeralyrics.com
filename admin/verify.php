<?php

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

$onLoginPage = true;
include_once $phpAbs . "admin/templates/header.php";

if( isset($_GET['email']) && isset($_GET['hash']) )
{
    // attempt authorization
    if( $user->attemptAuthorization( $_GET['email'], $_GET['hash'] ) )
    {
        echo 'Authorization successful. Please log in.';
    }

    else
    {
        echo 'Authorization failed.';
    }
}

?>

<?php
include_once $phpAbs . "admin/templates/footer.php";
?>