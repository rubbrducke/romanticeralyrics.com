<?php


/*====================================================================
    
    DOCUMENT HEADER

====================================================================*/

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "admin/templates/header.php";


/*====================================================================

    POST: EntryCommand

====================================================================*/

if( isset( $_POST['entryCommand'] ) )
{
    switch( $_POST['entryCommand'] )
    {
        case 'delete':
            $deleteStatement = $db->prepare( "DELETE FROM Publisher WHERE id=:id" );
            $deleteStatement->bindParam( ':id', $_POST['id'] );
            $deleteStatement->execute();
            break;
    }

    $user->redirect($htmlAbs . "admin/dashboard.php");

}


/*====================================================================
    
    POST: Everything Else

====================================================================*/

if( isset( $_POST['id'] ) )
{
    if( $_POST['id'] == 'new' )
    {
        $dbStatement = $db->prepare( "INSERT INTO Publisher ( name, city, address, start_year, end_year, description, description_sources ) VALUES ( :name, :city, :address, :start, :end, :description, :sources )" );
    
    }

    else
    {
        $dbStatement = $db->prepare( "UPDATE Publisher SET name=:name, city=:city, address=:address, start_year=:start, end_year=:end, description=:description, description_sources=:sources WHERE id=:id" );
        
        $dbStatement->bindParam( 'id', $_POST['id'] );

    }

    $dbStatement->bindParam( 'name', $_POST['name'] );
    $dbStatement->bindParam( 'city', $_POST['city'] );
    $dbStatement->bindParam( 'address', $_POST['address'] );
    $dbStatement->bindParam( 'start', $_POST['start'] );
    $dbStatement->bindParam( 'end', $_POST['end'] );
    $dbStatement->bindParam( 'description', $_POST['description'] );
    $dbStatement->bindParam( 'sources', $_POST['sources'] );

    $dbStatement->execute();


    /*====================================================================
        Redirect if newly inserted artist
    ====================================================================*/

    if( $_POST['id'] == 'new' )
    {
        $id = $db->getInsertId();
        $user->redirect( $htmlAbs . "admin/editor/publisher.php?id=$id" );
    
    }

}


/*====================================================================
    
    GET

====================================================================*/

if( isset( $_GET['id']) )
{
    $id = trim($_GET['id']);
    $id = strip_tags( $id );

    $dbStatement = $db->prepare("SELECT * FROM Publisher WHERE id=:id LIMIT 1");
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $results = $dbStatement->fetch(PDO::FETCH_ASSOC);

}


?>
<!--==================================================================

    FORM

===================================================================-->

<form id='info-form' method="post">


    <!--==================================================================
        Form Submission
    ===================================================================-->

    <?php if( ( $_GET['id'] == 'new' && $user->canCreate() ) || $user->canModify() ) : ?>

    <button class='primary' type="submit">Save</button>

    <?php endif; ?>


    <!--==================================================================
        Basic Information
    ===================================================================-->
    

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ID
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->     
    
    <input type='hidden' name='id' value="<?= $id ?>" />

    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Name
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    
    <h2>Name</h2>
    <input type="text" name="name" placeholder='Name' value="<?= $results['name'] ?>" />
    
    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        City
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    
    <h2>City</h2>
    <input type="text" name="city" placeholder='City' value="<?= $results['city'] ?>" />

    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Address
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    
    <h2>Address</h2>
    <input type="text" name="address" placeholder='Address' value="<?= $results['address'] ?>" />

    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Start Year
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    
    <h2>Start Year</h2>
    <input type="text" name="start" placeholder='YYYY' maxlength='4' value="<?= $results['start_year'] ?>" />

    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        End Year
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    
    <h2>End Year</h2>
    <input type="text" name="end" placeholder='YYYY' maxlength='4' value="<?= $results['end_year'] ?>" />

    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Description
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    
    <h2>Description</h2>
    <textarea rows='4' cols='50' name="description" /><?= $results['description'] ?></textarea>

    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Sources
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    
    <h2>Sources</h2>
    <textarea rows='4' cols='50' name="sources" /><?= $results['description_sources'] ?></textarea>


    <!--==================================================================
        Scores
    ===================================================================-->

    <h2>Individual Scores</h2>
    <div class='gallery'>
    <?php
    if( isset( $_GET['id'] ) && $_GET != 'new' )
    {
        $dbStatement = $db->prepare( "SELECT s.title, s.ref_id, s.work_id, s.id FROM Score s INNER JOIN Publisher_has_Score pScore ON s.id=pScore.score_id AND pScore.publisher_id=:pubID AND s.id NOT IN (SELECT score_id FROM Collection_has_Score)" );
        $dbStatement->bindParam( ':pubID', $id );
        $dbStatement->execute();

        $scores = $dbStatement->fetchAll(PDO::FETCH_ASSOC);

        if( $dbStatement->rowCount() > 0 )
        {
            foreach( $scores as $score )
            {
                if( $score['ref_id'] != '' )
                {
                    $refID = $score['ref_id'];
                    $scoreID = $score['id'];
                    $scoreTitle = $score['title'];

                    $image = getDirectoryThumbnail( "score_images/individual/$refID/thumbs/" );
                    generateHTMLThumbLink( $scoreTitle, $htmlAbs . "admin/editor/score.php?id=$scoreID", $image );

                } // if

            } // foreach

        } // if

        else
        {
            echo "<div class='flag-empty'>None</div>";

        } // else

    } // if
    ?>
    </div>


    <!--==================================================================
        Collections
    ===================================================================-->

    <h2>Collections</h2>
    <div class='gallery'>

    <?php
    if( isset( $_GET['id'] ) && $_GET != 'new' )
    {
        $dbStatement = $db->prepare( "SELECT c.title, c.id, c.ref_id, cS.index_start FROM Collection c INNER JOIN Collection_has_Score cS INNER JOIN Publisher_has_Collection pC ON pC.collection_id=c.id AND pC.publisher_id=:pubID GROUP BY c.id" );
        $dbStatement->bindParam( ':pubID', $id );
        $dbStatement->execute();

        $collections = $dbStatement->fetchAll(PDO::FETCH_ASSOC);

        if( $dbStatement->rowCount() > 0 )
        {
            foreach( $collections as $collection )
            {
                if( $collection['ref_id'] != '' )
                {
                    $refID = $collection['ref_id'];
                    $colID = $collection['id'];
                    $index = sprintf( '%03d', $collection['index_start'] );
                    $title = $collection['title'];

                    echo "<a href='" . $htmlAbs . "admin/editor/collection.php?id=$colID'>";
                    echo "<h3>$title</h3>";
                    echo "<img src='" . $htmlAbs . "score_images/$refID/thumbs/$refID" . "$index" . "_t.jpg'>";
                    echo "</a>";
                
                } // if

            } // foreach

        } // if

        else
        {
            echo "<div class='flag-empty'>None</div>";

        }

    } // if
    ?>

    </div>

</form>

<?php if( $user->canDelete() ) : ?>

<!--==================================================================
    
    DELETION FORM

===================================================================-->

<form id='delete-form' method='post'>
    <input type='hidden' name='id' value='<?= $id ?>' />
    <button class='caution' type='submit' name='entryCommand' value='delete'>Delete</button>
</form>


<!--==================================================================
    
    SCRIPTS

===================================================================-->

<script>
document.getElementById('delete-form').onclick = function()
{
    return confirm("Are you sure you want to delete this Publisher?");
};
</script>

<?php endif; ?>

<?php
include_once $phpAbs . "admin/templates/footer.php";
?>