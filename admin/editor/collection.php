<?php


/*====================================================================
    
    DOCUMENT HEADER

====================================================================*/

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "admin/templates/header.php";


/*====================================================================

    POST: EntryCommand

====================================================================*/

if( isset( $_POST['entryCommand'] ) )
{
    switch( $_POST['entryCommand'] )
    {
        case 'delete':
            $deleteStatement = $db->prepare( "DELETE FROM Collection WHERE id=:id" );
            $deleteStatement->bindParam( ':id', $_POST['id'] );
            $deleteStatement->execute();
            break;
    }

    $user->redirect($htmlAbs . "admin/dashboard.php");

}


/*====================================================================
    
    POST: Everything else

====================================================================*/

if( isset( $_POST['id'] ) )
{

    /*--------------------------------------------------------------------
        Establish a common refID
    --------------------------------------------------------------------*/

    if( isset($_POST['refID']) && $_POST['refID'] != '' )
    {
        $refID = $_POST['refID'];

    }

    else
    {
        $refID = strtolower($_POST['title']);
        $refID = preg_replace( '/[^a-zA-Z]+/', '', $refID );

    }


    /*====================================================================
        File upload
    ====================================================================*/

    if( isset( $_FILES['sheets']['size'] ) && $_FILES['sheets']['size'][0] > 0 )
    {
        $destination = $phpAbs . "score_images/$refID";
        uploadImages( $_FILES['sheets']['tmp_name'], $refID, $destination );

    }


    /*====================================================================
        Rename files
    ====================================================================*/

    if( isset( $_POST['images'] ) )
    {

        $images = $_POST['images'];

        $firstImage = array_slice( $images, 0, 1 );

        $filepath = $firstImage[0]['filename'];
        $filepath = str_replace( $htmlAbs, $phpAbs, $filepath );
        
        $collection = preg_replace( '/[0-9]*.jpg/', '', basename( $filepath ) );

        $jpegFolderPath = dirname( $filepath ) . '/';
        $thumbsFolderPath = dirname( dirname( $filepath ) ) . '/thumbs/';

        $tempPrefix = 'temp_';

        foreach( $images as $image )
        {
            $filepath = $image['filename'];
            $filepath = str_replace( $htmlAbs, $phpAbs, $filepath );

            $filename = basename( $filepath );

            $oldNumber = preg_replace( '/[^0-9]*/', '', $filename );
            $newNumber = sprintf( '%03d', $image['order'] );


            /*--------------------------------------------------------------------
                Update the filename if it has changed
            --------------------------------------------------------------------*/

            if( $oldNumber != $newNumber )
            {
                $oldJPEG = $filepath;
                $oldThumb = $thumbsFolderPath . $collection . $oldNumber . '_t.jpg';

                $newJPEG = $jpegFolderPath . $tempPrefix . $collection . $newNumber . '.jpg';
                $newThumb = $thumbsFolderPath . $tempPrefix . $collection . $newNumber . '_t.jpg';

                rename( $oldJPEG, $newJPEG );
                rename( $oldThumb, $newThumb );

            }

        }


        /*--------------------------------------------------------------------
            Remove 'temp_' from the filenames
        --------------------------------------------------------------------*/

        $jpegs = glob( "$jpegFolderPath/*$tempPrefix*[0-9].jpg" );
        foreach( $jpegs as $jpeg )
        {
            $newName = str_replace( $tempPrefix, '', $jpeg );
            rename( $jpeg, $newName );
            
        }

        $thumbs = glob( "$thumbsFolderPath/*temp_*[0-9]_t.jpg" );
        foreach( $thumbs as $thumb )
        {
            $newName = str_replace( $tempPrefix, '', $thumb );
            rename( $thumb, $newName );

        }

        /*====================================================================
            Insert / update scores
        ====================================================================*/

        if( isset( $_POST['scores'] ) )
        {
            $scores = $_POST['scores'];

            $scoreIDsToKeep = '';

            foreach( $scores as $score )
            {
                $scoreTitle = $score['title'];
                $scoreStart = $score['start'];
                $scorePageCount = $score['end'] - $scoreStart + 1;

                if( ! isset( $score['hasLink'] ) )
                {
                    $workStatement = $db->prepare( "INSERT INTO Work ( title ) VALUES ( :title )" );
                    $workStatement->bindParam( ':title', $scoreTitle );
                    $workStatement->execute();

                    $workID = $db->lastInsertId();

                    $scoreStatement = $db->prepare( "INSERT INTO Score ( title, work_id, page_count ) VALUES ( :title, :workID, :pageCount )" );
                    $scoreStatement->bindParam( ':title', $scoreTitle );
                    $scoreStatement->bindParam( ':workID', $workID );
                    $scoreStatement->bindParam( ':pageCount', $scorePageCount );
                    $scoreStatement->execute();

                    $scoreID = $db->lastInsertId();

                    $collectionScoreStatement = $db->prepare( "INSERT INTO Collection_has_Score ( Collection_id, Score_id, index_start ) VALUES ( :collectionID, :scoreID, :start )" );
                    $collectionScoreStatement->bindParam( ':collectionID', $_POST['id'] );
                    $collectionScoreStatement->bindParam( ':scoreID', $scoreID );
                    $collectionScoreStatement->bindParam( ':start', $scoreStart );
                    $collectionScoreStatement->execute();
                
                }

                else
                {
                    $scoreID = $score['hasLink'];

                    $collectionScoreUpdate = $db->prepare( 
                        "UPDATE Score 
                        SET title=:title 
                        WHERE Score.id=:scoreID" );
                    
                    $collectionScoreUpdate->bindParam( ':title', $scoreTitle );
                    $collectionScoreUpdate->bindParam( ':scoreID', $scoreID );
                    $collectionScoreUpdate->execute();

                }

                $scoreIDsToKeep .= " $scoreID,";

            }

            $scoreIDsToKeep = '(' . trim( $scoreIDsToKeep, ' ,' ) . ')';

            // Work Deletion Cascades to the Score
            // Score Deletion Cascades to the Collection

            $purgeScores = $db->prepare( 
                "DELETE FROM Work 
                WHERE id IN 
                    (SELECT work_id FROM Score 
                    WHERE Score.id IN 
                        (SELECT Score_id FROM Collection_has_Score 
                        WHERE Collection_id=:id) 
                    AND Score.id NOT IN $scoreIDsToKeep)" );

            $purgeScores->bindParam( ':id', $_POST['id'] );
            $purgeScores->execute();

            // Just to ensure everything we want deleted is, we'll sweep through the Collection_has_Score table too

            $purgeCollection = $db->prepare( 
                "DELETE FROM Collection_has_Score 
                WHERE Collection_id=:id 
                AND Score_id NOT IN $scoreIDsToKeep" );

            $purgeCollection->bindParam( ':id', $_POST['id'] );
            $purgeCollection->execute();

        }

    } // if $_POST['images']


    /*====================================================================
        Insert / Update Collection
    ====================================================================*/

    if( $_POST['id'] == 'new' )
    {
        $dbStatement = $db->prepare( "INSERT INTO Collection ( title, year, info, ref_id ) VALUES ( :title, :year, :info, :refID )" );

    }
    
    else
    {
        $dbStatement = $db->prepare( "UPDATE Collection SET title=:title, year=:year, info=:info, ref_id=:refID WHERE id=:id" );
        
        $id = (int) $_POST['id'];
        $dbStatement->bindParam( 'id', $id );
        
    }

    $dbStatement->bindParam( 'title', $_POST['title'] );
    $dbStatement->bindParam( 'year', $_POST['year'] );
    $dbStatement->bindParam( 'info', $_POST['info'] );
    $dbStatement->bindParam( 'refID', $refID );

    $dbStatement->execute();


    /*====================================================================
        Redirect if necessary
    ====================================================================*/

    if( $_POST['id'] == 'new' )
    {
        $collectionID = $_POST['id'] == 'new' ? $db->lastInsertId() : $_POST['id'];
        $user->redirect($htmlAbs . "admin/editor/collection.php?id=$collectionID");
    }
}


/*====================================================================
    
    GET

====================================================================*/

if( isset( $_GET['id']) )
{
    $id = trim($_GET['id']);
    $id = strip_tags( $id );


    /*--------------------------------------------------------------------
        Get this item
    --------------------------------------------------------------------*/

    $dbStatement = $db->prepare("SELECT * FROM Collection WHERE id=:id LIMIT 1");
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $results = $dbStatement->fetch(PDO::FETCH_ASSOC);


    /*--------------------------------------------------------------------
        Get Publisher
    --------------------------------------------------------------------*/

    $dbStatement = $db->prepare( "SELECT p.name, p.id FROM Publisher p INNER JOIN Publisher_has_Collection pC ON pC.collection_id=:id");
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $publisher = $dbStatement->fetch(PDO::FETCH_ASSOC);


    /*--------------------------------------------------------------------
        Get information for images
    --------------------------------------------------------------------*/

    $dbStatement = $db->prepare( "SELECT s.title, s.id, s.page_count, cS.index_start FROM Score s INNER JOIN Collection_has_Score cS ON cS.Score_id=s.id AND collection_id=:collectionID" );
    $dbStatement->bindParam( ':collectionID', $id );
    $dbStatement->execute();

    $scores = $dbStatement->fetchAll(PDO::FETCH_ASSOC);
}

?>


<!--==================================================================

    FORM

===================================================================-->

<form id='info-form' method="post" enctype="multipart/form-data">


    <!--==================================================================
        Form Submission
    ===================================================================-->

    <?php if( ( $_GET['id'] == 'new' && $user->canCreate() ) || $user->canModify() ) : ?>

    <button class='primary' type="submit">Save</button>

    <?php endif; ?>


    <!--==================================================================
        Basic Information
    ===================================================================-->


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ID and RefID
        
        Hidden fields that store the database id and refID of this item.
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <input type='hidden' name='id' value="<?= $id ?>" />
    <input type='hidden' name='refID' value="<?= $results['ref_id'] ?>">
    
    <h2>Publisher</h2>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Publisher
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    

    <?php if( $_GET['id'] != 'new' ) : ?>

    <div class='linkedInput'>

        <input type='text' list='publishers' name='publisher' value="<?= $publisher['name'] ?>" >
        
        <a target='_blank' href="<?=$htmlAbs?>admin/editor/publisher.php?id=<?= $publisher['id'] ?>" class='score-link'>
            <img src="<?=$htmlAbs?>admin/editor/images/documentIcon.png">
        </a>

    </div>

    <?php else : ?>

    <input type='text' list='publishers' name='publisher' value="<?= $publisher['name'] ?>" >

    <?php endif; ?>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Publisher List
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <datalist id='publishers'>

    <?php


    // Populate the publisher datalist

    $dbStatement = $db->prepare( "SELECT name FROM Publisher ORDER BY name ASC" );
    $dbStatement->execute();

    $publishers = $dbStatement->fetchAll(PDO::FETCH_ASSOC);

    foreach( $publishers as $publisher )
    {
        $name = $publisher['name'];
        echo "<option value='$name'>";
    }
    ?>

    </datalist>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Title
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Title</h2>
    <input type="text" name="title" value="<?= $results['title'] ?>" required />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Year
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Year</h2>
    <input type="text" name="year" value="<?= $results['year'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Info
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Info</h2>
    <input type="text" name="info" value="<?= $results['info'] ?>" />


    <!--==================================================================
        Image Upload
    ===================================================================-->

    <h2>Images</h2>

    <?php if( $user->canUpload() ) : ?>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Upload
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <label>Select one or more photos</label>
    <input type='file' name='sheets[]' multiple />

    <?php endif; ?>


    <!--==================================================================
        Collection Scores
    ===================================================================-->
    
    <div class='collection'>

        <?php


        // Get the referenceID / folder name

        $refID = $results['ref_id'];


        // Directories for thumbnails and jpegs

        $lowResSearch = $phpAbs . "score_images/$refID/thumbs/*[0-9]_t.jpg";
        $highResSearch = $phpAbs . "score_images/$refID/jpeg/*[0-9].jpg";

        
        // Use glob() to get the folder contents

        $lowResImages = glob( $lowResSearch ); 
        $highResImages = glob( $highResSearch );
        ?>


        <!-- Images -->

        <ul class='images sortable' style='height: <?php echo sizeof($lowResImages)*400 ?>'>
        
        <?php


        /*--------------------------------------------------------------------
            Iterate images
        --------------------------------------------------------------------*/

        foreach( $lowResImages as $index => $lowResImage ) :

            $lowResImage = str_replace( $phpAbs, $htmlAbs, $lowResImage );
            $highResImage = str_replace( $phpAbs, $htmlAbs, $highResImages[$index] );
            ?>

            <li href='<?= $highResImage . '?' . date_timestamp_get( date_create() ) ?>'>
                
                <!-- Thumbnail -->

                <img class='collection-image' src='<?= $lowResImage . '?' . date_timestamp_get( date_create() ) ?>' />
                
                <!-- Filename -->

                <input type='hidden' name='images[<?= $index + 1 ?>][filename]' value='<?= $highResImage ?>' />
                
                <!-- Order -->

                <input type='hidden' name='images[<?= $index + 1 ?>][order]' value='<?= $index + 1 ?>' />

            </li>

        <?php endforeach; ?>

        </ul>


        <!-- Associated Scores -->

        <ul class='scores' style='height: <?php echo sizeof($lowResImages)*405 ?>'>

        <?php 


        /*--------------------------------------------------------------------
            CREATE SCORE-BRACKETS EDITOR
        --------------------------------------------------------------------*/

        // Used to iterate the input array

        $scoreIndex = 0;

        // Used to identify if the current page is contained in a previous score-bracket
        
        $lastPageOverlapped = 0;

        for( $pageNum = 1; $pageNum <= sizeof($lowResImages); $pageNum++ ) : 


            /*--------------------------------------------------------------------
                FIND SCORES FOR PAGE
            --------------------------------------------------------------------*/

            $singlePageMatches = array();
            $multiPageMatches = array();

            foreach( $scores as $score )
            {
                if( $score['index_start'] == ($pageNum) )
                {
                    $score['page_count'] > 1 ? array_push( $multiPageMatches, $score ) : array_push( $singlePageMatches, $score );
                }
            }

            $scoreHeight = 405;
            $maxBracketHeight = 390;
            $bracketHeight = 120;
            $bracketMargin = 15;
            $iterateBracket = $bracketHeight + $bracketMargin;

            /*--------------------------------------------------------------------

                |----------------| ---------|         -         -
                |                |  bracket |  120    |         |
                |                | ---------|         |         |
                |                |    15              |         |
                |                | ---------|         |         |
                |      score     |  bracket |  120    |  390    |
                |                | ---------|         |         |  405
                |                |    15              |         |
                |                | ---------|         |         |
                |                |  bracket |  120    |         |
                |----------------| ---------|         -         |
                        15                                      |
                |----------------| ---------|         -         -
                |                |  bracket |         |         |

            --------------------------------------------------------------------*/


            /*--------------------------------------------------------------------
                CREATE TOP BRACKET
            --------------------------------------------------------------------*/

            $topIsObstructed = ($lastPageOverlapped > $pageNum) || (sizeof($singlePageMatches) == 0 && sizeof($multiPageMatches) > 0) ? 'obstructed' : '';
            $bottomIsObstructed = $lastPageOverlapped > $pageNum ? 'obstructed' : '';

            // Create <li>

            if( $lastPageOverlapped == $pageNum && sizeof($multiPageMatches) > 0 ) : ?>

                <li class='score-bracket top-bracket <?= $topIsObstructed ?>' data-page='<?= $pageNum ?>' style='top: <?= ($pageNum - 1) * $scoreHeight + $iterateBracket ?>; height: <?= $bracketHeight ?>'>

            <?php elseif( $lastPageOverlapped == $pageNum ) : ?>

                <li class='score-bracket top-bracket <?= $topIsObstructed ?>' data-page='<?= $pageNum ?>' style='top: <?= ($pageNum - 1) * $scoreHeight + $iterateBracket ?>; height: <?= $bracketHeight + $iterateBracket ?>'>

            <?php elseif( sizeof($multiPageMatches) > 0 ) : ?>

                <li class='score-bracket top-bracket <?= $topIsObstructed ?>' data-page='<?= $pageNum ?>' style='top: <?= ($pageNum - 1) * $scoreHeight ?>; height: <?= $bracketHeight + $iterateBracket ?>'>

            <?php else : ?>

                <li class='score-bracket top-bracket <?= $topIsObstructed ?>' data-page='<?= $pageNum ?>' style='top: <?= ($pageNum - 1) * $scoreHeight ?>; height: <?= $maxBracketHeight ?>'>

            <?php endif; ?>

            <?php

            // Fill out <li>

            if( sizeof($singlePageMatches) > 0 ) :

                foreach( $singlePageMatches as $match ) : 
                    $scoreIndex++;
                ?>

                    <div class='score'>

                        <div class='linkedInput'>


                            <!-- Title -->

                            <input type='text' class='score-title' name='scores[<?= $scoreIndex ?>][title]' value="<?= $match['title'] ?>" placeholder="Score Title">
                            

                            <!-- Link Icon -->

                            <a target='_blank' href="<?=$htmlAbs?>admin/editor/score.php?id=<?= $match['id'] ?>&ref=<?= $results['ref_id'] ?>&start=<?= $pageNum ?>" class='score-link'>
                                <img src="<?=$htmlAbs?>admin/editor/images/documentIcon.png">
                            </a>

                        </div>


                        <!-- Link ID -->

                        <input type='hidden' class='score-exists' name='scores[<?= $scoreIndex ?>][hasLink]' value='<?= $match['id'] ?>'>
                        

                        <!-- Page Start -->

                        <input type='hidden' class='score-start' name='scores[<?= $scoreIndex ?>][start]' value='<?= $pageNum ?>'>
                        

                        <!-- Page End -->

                        <input type='hidden' class='score-end' name='scores[<?= $scoreIndex ?>][end]' value='<?= $pageNum + $match['page_count'] - 1 ?>'>
                        
                    </div>

                <?php 
                endforeach;

                for( $c = 0; $c < (3 - sizeof($singlePageMatches)) && $c < 3; $c++ ) : 
                    $scoreIndex++; 
                ?>

                    <div class='score'>
                        
                        <input type='text' class='score-title' name='scores[<?= $scoreIndex ?>][title]' value='' placeholder="Score Title">
                        
                        <input type='hidden' class='score-start' name='scores[<?= $scoreIndex ?>][start]' value='<?= $pageNum ?>'>
                        
                        <input type='hidden' class='score-end' name='scores[<?= $scoreIndex ?>][end]' value='<?= $pageNum ?>'>
                    
                    </div>

                <?php endfor; ?>

            <?php else : ?>

                <?php for( $c = 0; $c < 3; $c++ ) : 
                     $scoreIndex++; 
                ?>

                    <div class='score'>
                        
                        <input type='text' class='score-title' name='scores[<?= $scoreIndex ?>][title]' value="" placeholder="Score Title">
                        
                        <input type='hidden' class='score-start' name='scores[<?= $scoreIndex ?>][start]' value='<?= $pageNum ?>'>
                        
                        <input type='hidden' class='score-end' name='scores[<?= $scoreIndex ?>][end]' value='<?= $pageNum ?>'>
                    
                    </div>

                <?php endfor; ?>

            <?php endif; ?>

            <div class='vertical-bar'></div>
            <div class='add-remove minus'></div>

            </li>

            <?php


            /*--------------------------------------------------------------------
                CREATE BOTTOM BRACKET
            --------------------------------------------------------------------*/

            if( sizeof($multiPageMatches) > 0 && $bottomIsObstructed != 'obstructed' )
            {
                $lastPageOverlapped = $pageNum + $multiPageMatches[0]['page_count'] - 1;
            }

            if( sizeof($multiPageMatches) > 0 ) : 

                $fullPages = $multiPageMatches[0]['page_count'] - 2;
            ?>

                <?php if( sizeof($singlePageMatches) > 0 ) : ?>

                    <li 
                    class='score-bracket bottom-bracket <?= $bottomIsObstructed ?>' 
                    data-page='<?= $pageNum ?>' 
                    style=' top: <?= ( ($pageNum - 1) * $scoreHeight) + ($iterateBracket * 2) ?>; 
                            height: <?= $bracketHeight + max($fullPages, 0) * $maxBracketHeight + $iterateBracket ?>'
                    >

                <?php else : ?>

                    <li 
                    class='score-bracket bottom-bracket <?= $bottomIsObstructed ?>' 
                    data-page='<?= $pageNum ?>' 
                    style=' top: <?= ($pageNum - 1) * $scoreHeight ?>; 
                            height: <?= $maxBracketHeight + max($fullPages, 0) * $maxBracketHeight + $iterateBracket ?>'
                    >

                <?php endif; ?>

                <?php foreach( $multiPageMatches as $match ) : 
                     $scoreIndex++;
                ?>

                    <div class='score'>

                        <div class='linkedInput'>

                            <!-- Title -->

                            <input 
                            type='text' class='score-title' name='scores[<?= $scoreIndex ?>][title]' value="<?= $match['title'] ?>" placeholder="Score Title">


                            <!-- Link Icon -->
                            
                            <a target='_blank' href="<?=$htmlAbs?>admin/editor/score.php?id=<?= $match['id'] ?>&ref=<?= $results['ref_id'] ?>&start=<?= $pageNum ?>" class='score-link'>
                                <img src="<?=$htmlAbs?>admin/editor/images/documentIcon.png">
                            </a>

                        </div>


                        <!-- Link ID -->
                        
                        <input type='hidden' class='score-exists' name='scores[<?= $scoreIndex ?>][hasLink]' value='<?= $match['id'] ?>'>


                        <!-- Page Start -->
                        
                        <input type='hidden' class='score-start' name='scores[<?= $scoreIndex ?>][start]' value='<?= $pageNum ?>'>
                        

                        <!-- Page End -->

                        <input type='hidden' class='score-end' name='scores[<?= $scoreIndex ?>][end]' value='<?= $pageNum + $match['page_count'] - 1 ?>'>

                    </div>

                <?php endforeach; ?>

                <?php for( $c = 0; $c < (3 - sizeof($multiPageMatches)) && $c < 3; $c++ ) : 
                    $scoreIndex++; 
                ?>

                    <div class='score'>
                        
                        <input type='text' class='score-title' name='scores[<?= $scoreIndex ?>][title]' value="" placeholder="Score Title">
                        
                        <input type='hidden' class='score-start' name='scores[<?= $scoreIndex ?>][start]' value='<?= $pageNum ?>'>
                        
                        <input type='hidden' class='score-end' name='scores[<?= $scoreIndex ?>][end]' value='<?= $pageNum ?>'>
                    
                    </div>

                <?php endfor; ?>

            <?php else : ?>

                <li class='score-bracket bottom-bracket form-ignore obstructed' data-page='<?= $pageNum ?>' style='top: <?= ( ($pageNum - 1) * $scoreHeight) + ($iterateBracket * 2) ?>; height: 120px'>

                <?php for( $c = 0; $c < 3; $c++ ) : 
                     $scoreIndex++; 
                ?>

                    <div class='score'>
                        
                        <input type='text' class='score-title' name='scores[<?= $scoreIndex ?>][title]' value="" placeholder="Score Title">
                        
                        <input type='hidden' class='score-start' name='scores[<?= $scoreIndex ?>][start]' value='<?= $pageNum ?>'>
                        
                        <input type='hidden' class='score-end' name='scores[<?= $scoreIndex ?>][end]' value='<?= $pageNum ?>'>
                    
                    </div>

                <?php endfor; ?>

            <?php endif; ?>

            <div class='vertical-bar'></div>
            <div class='add-remove minus'></div>

            </li>

        <?php endfor; ?>

        </ul>

    </div>

</form>

<?php if( $user->canDelete() ) : ?>

<!--==================================================================
    
    DELETION FORM

===================================================================-->

<form id='delete-form' method='post'>
    <input type='hidden' name='id' value='<?= $id ?>' />
    <button class='caution' type='submit' name='entryCommand' value='delete'>Delete</button>
</form>

<?php endif; ?>

<!--==================================================================
    
    SCRIPTS

===================================================================-->

<script src="<?=$htmlAbs?>admin/libs/jquery-1.7.1.min.js"></script>
<script src="<?=$htmlAbs?>admin/libs/jquery-ui.min.js"></script>
<script src="<?=$htmlAbs?>admin/libs/jquery.magnific-popup.min.js"></script>

<script>
window.onload = function()
{
    if( window.jQuery )
    {
        $( function() {


            /*====================================================================
                Submit Listener
            ====================================================================*/

            $('#info-form').submit( function(e){

                /*--------------------------------------------------------------------
                    Remove empty score inputs
                --------------------------------------------------------------------*/

                $( '.score .score-title' ).each( function( index, value )
                {
                    if( $(this).val().trim() == '' )
                    {  
                        $(this).closest( '.score' ).remove();
                    }

                } );

                return true;

            });

            $('#delete-form').submit( function(e){
                return confirm("Deleting the collection will also delete the Scores and images. \nAre you sure you want to delete this Collection?");
            });

            /*====================================================================
                Magnific Popup
            ====================================================================*/

            $( '.collection .images li' ).magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: false,
                fixedContentPos: true,
                mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                image: {
                    verticalFit: true
                },
                zoom: {
                    enabled: true,
                    duration: 300 // don't foget to change the duration also in CSS
                }
            });

            /*====================================================================
                Sortable and Resizable
            ====================================================================*/

            $( ".sortable" ).sortable( 
                {
                    stop: function( e, ui )
                    {
                        $(this).children().each( function( index, value )
                        {
                            $(value).find( 'input:nth-of-type(2)' ).val( index + 1 );
                        } );
                    }
                }
            );

            $( '.score-bracket' ).resizable(
                { 
                    handles: 'n, s',
                    containment: 'parent',

                    /*--------------------------------------------------------------------
                        ON START
                    --------------------------------------------------------------------*/
                    
                    start: function( e, ui )
                    {
                        $(this).siblings('.score-bracket').not('.obstructed').css('opacity', '0.25');
                    },

                    /*--------------------------------------------------------------------
                        ON RESIZE
                    --------------------------------------------------------------------*/

                    resize: function( e, ui )
                    {

                        // Initialize Grid Variables

                        var scoreHeight = 405;
                        var bracketHeight = 120;
                        var bracketMargin = 15;
                        var snapHeight = bracketHeight + bracketMargin;


                        // Get current position and height

                        var posTop = ui.position.top;
                        var posBottom = ui.position.top + ui.size.height;
                        var currentHeight = ui.size.height;


                        if( $(this).hasClass('top-bracket') )
                        {
                            var borderTop = ($(this).data('page') - 1) * scoreHeight;
                            var borderOneThird = borderTop + snapHeight;
                            var borderTwoThird = borderOneThird + bracketHeight;

                            if( currentHeight < bracketHeight || posTop < borderTop || posTop > borderOneThird || posBottom < borderTwoThird )
                            {
                                $(this).trigger('mouseup');
                            }

                        }


                        else if( $(this).hasClass('bottom-bracket') )
                        {
                            var borderTop = ($(this).data('page') - 1) * scoreHeight + snapHeight + snapHeight;
                            var borderBottom = borderTop + snapHeight;

                            if( currentHeight < bracketHeight || posTop < borderTop || posTop > borderBottom )
                            {
                                $(this).trigger('mouseup');
                            }
                        }

                    },

                    /*--------------------------------------------------------------------
                        ON STOP
                    --------------------------------------------------------------------*/

                    stop: function( e, ui )
                    {

                        // Reset opacity
                        
                        $(this).siblings('.score-bracket').css('opacity', '');

                        // Initialize Measurements

                        var top = ui.helper.position().top;
                        var height = ui.helper.height();

                        var scoreHeight = 405;
                        var bracketHeight = 120;
                        var bracketMargin = 15;
                        var snapHeight = bracketHeight + bracketMargin;


                        // Snap the UI element to the grid defined by the snapHeight

                        ui.helper.css( {
                            top: Math.round( top / snapHeight ) * snapHeight,
                            height: Math.round( height / snapHeight ) > 1 ? Math.round( height / snapHeight ) * snapHeight - 15 : bracketHeight
                        } );


                        // Calculate original top and bottom offsets

                        var originalTop = ($(this).data('page') - 1) * scoreHeight;
                        var originalBottom = originalTop + scoreHeight - 15;
                        
                        
                        // Calculate current height, and top and bottom offsets

                        var currentHeight = Math.round( ui.size.height / snapHeight ) > 1 ? Math.round( ui.size.height / snapHeight ) * snapHeight - 15 : bracketHeight;
                        
                        var currentTop = ui.position.top;
                        var currentBottom = ui.position.top + currentHeight;

                        
                        // Calculate the number of grid units from the top and bottom

                        var snapsFromTop = Math.round( (currentTop - originalTop) / snapHeight );
                        var snapsFromBottom = Math.round( (currentBottom - originalBottom) / snapHeight );


                        // Calculate the distance from the bottom in pages

                        var pagesFromBottom = Math.ceil( snapsFromBottom / 3 );


                        // Calculate the previous page count

                        var oldPageCount = $(this).first().find('input.score-end').val() - $(this).first().find('input.score-start').val();

                        var topBrackets = pagesFromBottom;
                        var prevTopBrackets = oldPageCount;


                        // Hide all of the things

                        var sizeCheckOne = currentTop == originalTop && currentHeight > bracketHeight + snapHeight;

                        var sizeCheckTwo = currentTop > originalTop && currentHeight >= bracketHeight + snapHeight;

                        if( $(this).hasClass( 'top-bracket' ) && (sizeCheckOne || sizeCheckTwo) )
                        {
                            $(this).next().addClass( 'obstructed' );
                            
                        }

                        else
                        {
                            $(this).next().removeClass( 'obstructed' );

                        }

                        $(this).nextAll( '.top-bracket' ).slice( 0, topBrackets )
                            .addClass( 'obstructed' )
                            .next().addClass( 'obstructed' );
                        

                        /*--------------------------------------------------------------------
                            STOP AT FIRST SNAP POSITION
                        --------------------------------------------------------------------*/

                        var snapPosition = snapsFromBottom - ((pagesFromBottom - 1) * 3);
                        
                        if( snapPosition == 1 )
                        {

                            if( currentHeight > scoreHeight )
                            {
                                var followingTopBracket = $(this).nextAll( '.top-bracket' ).slice( topBrackets - 1, topBrackets );
                            }

                            else if( $(this).hasClass( 'top-bracket' ) )
                            {
                                var followingTopBracket = $(this).next().next();
                            }

                            else
                            {
                                var followingTopBracket = $(this).next();
                            }

                            $(followingTopBracket).removeClass( 'obstructed' );

                            var top = parseInt( $(followingTopBracket).css('top'), 10 );
                            var height = parseInt( $(followingTopBracket).css('height'), 10 );

                            var newTop = top + snapHeight;
                            var newHeight = height - snapHeight;

                            if( top == originalTop + (scoreHeight * pagesFromBottom) )
                            {
                                $(followingTopBracket).css( 'top', newTop );
                                $(followingTopBracket).css( 'height', newHeight );

                                if( newHeight < bracketHeight + snapHeight )
                                {
                                    $(followingTopBracket).next().removeClass( 'obstructed' );
                                }

                            }
                            

                        }


                        /*--------------------------------------------------------------------
                            STOP AT SECOND SNAP POSITION
                        --------------------------------------------------------------------*/

                        else if( snapPosition == 2 )
                        {

                            // Sibling of the last hidden bracket becomes visible

                            $(this).nextAll( '.top-bracket' ).slice( topBrackets - 1, topBrackets )
                                .next().removeClass( 'obstructed' );

                        }


                        /*--------------------------------------------------------------------
                            STOP AT THIRD SNAP POSITION
                        --------------------------------------------------------------------*/

                        else if( snapPosition == 3 )
                        {

                            // Ensure next top-bracket is visible

                            if( currentHeight > scoreHeight )
                            {
                                var nextTop = $(this).nextAll( '.top-bracket' ).slice( topBrackets - 1, topBrackets ).next().next();
                            }

                            else if( $(this).hasClass( 'top-bracket' ) )
                            {
                                var nextTop = $(this).next().next();
                            }

                            else
                            {
                                var nextTop = $(this).next();
                            }

                            $(nextTop).removeClass( 'obstructed' );

                            var nextBottom = $(nextTop).next();

                            currentTop = parseInt( $(nextTop).css('top'), 10 );
                            currentHeight = parseInt( $(nextTop).css('height'), 10 );

                            originalTop = originalTop + (scoreHeight * pagesFromBottom) + scoreHeight;
                            
                            var sizeCheckOne = 
                                currentTop == originalTop
                                && currentHeight > bracketHeight + snapHeight;

                            var sizeCheckTwo = 
                                currentTop > originalTop 
                                && currentHeight > bracketHeight;

                            if( sizeCheckOne || sizeCheckTwo )
                            {
                                $(nextBottom).addClass( 'obstructed' );
                                
                            }

                            else
                            {
                                $(nextBottom).removeClass( 'obstructed' );

                            }
                        }
                        
                        $(this).children().children( '.score-end' ).val( $(this).data('page') + pagesFromBottom );

                        e.stopImmediatePropagation();

                    } // stop

                } // args

            ); // .resizable()


            /*====================================================================
                Add and Remove Buttons
            ====================================================================*/

            $( '.add-remove' ).click( function() 
            {
                $(this).toggleClass('plus').toggleClass('minus');
                $(this).siblings().toggleClass('obstructed');
            });

            $( '.score-bracket' ).each( function( key, elem ){
                if( $(elem).find('input').val() == '' )
                {
                    $(this).children( '.add-remove' ).toggleClass('plus').toggleClass('minus');
                    $(this).children( '.add-remove' ).siblings().toggleClass( 'obstructed' );
                }
            });


        } ); // function

    } // if

} // window.onload

</script>

<?php
include_once $phpAbs . "admin/templates/footer.php";
?>