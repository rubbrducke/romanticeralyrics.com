<?php


/*====================================================================
    
    DOCUMENT HEADER

====================================================================*/

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "admin/templates/header.php";


/*====================================================================

    POST: EntryCommand

====================================================================*/

if( isset( $_POST['entryCommand'] ) )
{
    switch( $_POST['entryCommand'] )
    {
        case 'delete':
            $deleteStatement = $db->prepare( "DELETE FROM Artist WHERE id=:id" );
            $deleteStatement->bindParam( ':id', $_POST['id'] );
            $deleteStatement->execute();
            break;
    }

    $user->redirect($htmlAbs . "admin/dashboard.php");

}


/*====================================================================

    POST: Everything Else

====================================================================*/

if( isset( $_POST['id'] ) )
{


    /*====================================================================
        Prepare PDO Statement
    ====================================================================*/

    if( $_POST['id'] == 'new' )
    {
        $dbStatement = $db->prepare( "INSERT INTO Artist ( title, f_name, m_name, l_name, suffix, type, born, died, biography, biography_sources ) VALUES ( :title, :first, :middle, :last, :suffix, :type, :born, :died, :biography, :sources )" );
    
    }

    else
    {
        $dbStatement = $db->prepare( "UPDATE Artist SET title=:title, f_name=:first, m_name=:middle, l_name=:last, suffix=:suffix, type=:type, born=:born, died=:died, biography=:biography, biography_sources=:sources WHERE id=:id" );
        
        $id = (int) $_POST['id'];
        $dbStatement->bindParam( 'id', $id );
        
    }


    /*====================================================================
        Bind parameters
    ====================================================================*/

    $birth = $_POST['birthYear'] . '-' . $_POST['birthMonth'] . '-' . $_POST['birthDay'];
    $death = $_POST['deathYear'] . '-' . $_POST['deathMonth'] . '-' . $_POST['deathDay'];

    $dbStatement->bindParam( 'title', $_POST['title'] );
    $dbStatement->bindParam( 'first', $_POST['first'] );
    $dbStatement->bindParam( 'middle', $_POST['middle'] );
    $dbStatement->bindParam( 'last', $_POST['last'] );
    $dbStatement->bindParam( 'suffix', $_POST['suffix'] );
    $dbStatement->bindParam( 'type', $_POST['profession'] );
    $dbStatement->bindParam( 'born', $birth );
    $dbStatement->bindParam( 'died', $death );
    $dbStatement->bindParam( 'biography', $_POST['biography'] );
    $dbStatement->bindParam( 'sources', $_POST['sources'] );

    $dbStatement->execute();


    /*====================================================================
        Redirect if newly inserted artist
    ====================================================================*/

    if( $_POST['id'] == 'new' )
    {
        $id = $db->getInsertId();
        $user->redirect( $htmlAbs . "admin/editor/artist.php?id=$id" );
    
    }
    
}


/*====================================================================
    
    GET

====================================================================*/

if( isset( $_GET['id']) )
{
    $id = trim($_GET['id']);
    $id = strip_tags( $id );

    $dbStatement = $db->prepare("SELECT * FROM Artist WHERE id=:id LIMIT 1");
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $results = $dbStatement->fetch(PDO::FETCH_ASSOC);
    
}

?>


<!--==================================================================
   
    FORM

===================================================================-->

<form method="post">


    <!--==================================================================
        Form Submission
    ===================================================================-->

    <?php if( ( $_GET['id'] == 'new' && $user->canCreate() ) || $user->canModify() ) : ?>

    <button class='primary' type="submit">Save</button>

    <?php endif; ?>


    <!--==================================================================
        Basic Information
    ===================================================================-->


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ID
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <input type='hidden' name='id' value="<?= $id ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Title
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Name</h2>
    <input type="text" name="title" value="<?= $results['title'] ?>" placeholder='Title' class='col5'/>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        First Name
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <input type="text" name="first" placeholder='First' value="<?= $results['f_name'] ?>" class='col5' />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Middle Name
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <input type="text" name="middle" placeholder='Middle' value="<?= $results['m_name'] ?>" class='col5' />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Last Name
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <input type="text" name="last" placeholder='Last' value="<?= $results['l_name'] ?>" class='col5' />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Suffix
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <input type="text" name="suffix" placeholder='Suffix' value="<?= $results['suffix'] ?>" class='col5' />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Profession
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Profession</h2>
    <input type="text" name="type" placeholder='Composer / Lyricist / Etc.' value="<?= $results['type'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Birth
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <?php
        $year = ''; 
        $month = ''; 
        $day = '';

        $date = explode( '-', $results['born'] );
        if( sizeof( $date ) == 3 )
        {
            $year = $date[0];
            $month = $date[1];
            $day = $date[2];
        }
    ?>

    <h2>Birth</h2>
    <fieldset class="date">
        <input type="text" name="birthMonth" placeholder="MM" maxlength="2" value='<?= $month ?>' />
        <input type="text" name="birthDay" placeholder="DD" maxlength="2" value='<?= $day ?>' />
        <input type="text" name="birthYear" placeholder="YYYY" maxlength="4" value='<?= $year ?>' />
    </fieldset>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Death
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <?php
        $year = ''; 
        $month = ''; 
        $day = '';

        $date = explode( '-', $results['died'] );
        if( sizeof( $date ) == 3 )
        {
            $year = $date[0];
            $month = $date[1];
            $day = $date[2];
        }
    ?>

    <h2>Death</h2>
    <fieldset class="date">
        <input type="text" name="deathMonth" placeholder="MM" maxlength="2" value='<?= $month ?>' />
        <input type="text" name="deathDay" placeholder="DD" maxlength="2" value='<?= $day ?>' />
        <input type="text" name="deathYear" placeholder="YYYY" maxlength="4" value='<?= $year ?>' />
    </fieldset>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Biography
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Biography</h2>
    <textarea rows='4' cols='50' name="biography" value="<?= $results['biography'] ?>" /></textarea>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Sources
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Sources</h2>
    <textarea rows='4' cols='50' name="sources" value="<?= $results['biography_sources'] ?>" /></textarea>


    <!--==================================================================
        Scores
    ===================================================================-->

    <h2>Individual Scores</h2>
    <div class='gallery'>
    <?php
    if( isset( $_GET['id'] ) && $_GET != 'new' )
    {
        $dbStatement = $db->prepare( "SELECT s.title, s.ref_id, s.work_id, s.id FROM Score s INNER JOIN Artist_has_Score aScore ON s.id=aScore.score_id AND aScore.artist_id=:artistID AND s.id NOT IN (SELECT score_id FROM Collection_has_Score)" );
        $dbStatement->bindParam( ':artistID', $id );
        $dbStatement->execute();

        $results = $dbStatement->fetchAll(PDO::FETCH_ASSOC);

        if( $dbStatement->rowCount() > 0 )
        {
            foreach( $results as $result )
            {
                if( $result['ref_id'] != '' )
                {
                    $refID = $result['ref_id'];
                    $scoreID = $result['id'];
                    $scoreTitle = $result['title'];

                    $image = getDirectoryThumbnail( "score_images/individual/$refID/thumbs/" );
                    generateHTMLThumbLink( $scoreTitle, $htmlAbs . "admin/editor/score.php?id=$scoreID", $image );
                }
            }
        }

        else
        {
            echo "<div class='flag-empty'>None</div>";
        }
    }
    ?>
    </div>


    <!--==================================================================
        Works
    ===================================================================-->

    <h2>Works</h2>
    <div class='gallery'>
    <?php
    if( isset( $_GET['id'] ) && $_GET != 'new' )
    {
        $dbStatement = $db->prepare( "SELECT w.id, s.title, s.ref_id, w.title FROM Artist a INNER JOIN Artist_has_Work aWork ON a.id=aWork.artist_id INNER JOIN Work w ON aWork.work_id=w.id INNER JOIN Score s ON w.id=s.work_id AND a.id=:artistID AND s.id NOT IN (SELECT score_id FROM Collection_has_Score)" );
        $dbStatement->bindParam( ':artistID', $id );
        $dbStatement->execute();

        $results = $dbStatement->fetchAll(PDO::FETCH_ASSOC);

        if( $dbStatement->rowCount() > 0 )
        {
            foreach( $results as $result )
            {
                if( $result['ref_id'] != '' )
                {
                    $refID = $result['ref_id'];
                    $scoreID = $result['id'];
                    $scoreTitle = $result['title'];

                    $image = getDirectoryThumbnail( "score_images/individual/$refID/thumbs" );
                    generateHTMLThumbLink( $scoreTitle, $htmlAbs . "admin/editor/score.php?id=$scoreID", $image );
                }
            }
        }

        else
        {
            echo "<div class='flag-empty'>None</div>";
        }

    }
    ?>
    </div>
    

    <!--==================================================================
        Collections
    ===================================================================-->

    <h2>Participant of Collections</h2>
    <div class='gallery'>

    <?php
    if( isset( $_GET['id'] ) && $_GET != 'new' )
    {
        $dbStatement = $db->prepare( "SELECT c.title, c.id, c.ref_id, cScore.index_start FROM Collection c INNER JOIN Collection_has_Score cScore INNER JOIN Artist_has_Score aScore ON cScore.score_id=aScore.score_id AND aScore.artist_id=:artistID GROUP BY c.title" );
        $dbStatement->bindParam( ':artistID', $id );
        $dbStatement->execute();

        $collections = $dbStatement->fetchAll(PDO::FETCH_ASSOC);

        if( $dbStatement->rowCount() > 0 )
        {
            foreach( $collections as $collection )
            {
                if( $collection['ref_id'] != '' )
                {
                    $refID = $collection['ref_id'];
                    $colID = $collection['id'];
                    $index = $collection['index_start'];
                    $title = $collection['title'];

                    $image = getDirectoryThumbnail( "score_images/$refID/thumbs/", $index );
                    generateHTMLThumbLink( $title, $htmlAbs . "admin/editor/collection.php?id=$colID", $image );
                
                } // if

            } // foreach

        } // if

        else
        {
            echo "<div class='flag-empty'>None</div>";
        }

    } // if
    ?>

    </div>

</form>

<?php if( $user->canDelete() ) : ?>

<!--==================================================================
    
    DELETION FORM

===================================================================-->

<form id='delete-form' method='post'>
    <input type='hidden' name='id' value='<?= $id ?>' />
    <button class='caution' type='submit' name='entryCommand' value='delete'>Delete</button>
</form>

<!--==================================================================
    
    SCRIPTS

===================================================================-->

<script>
document.getElementById('delete-form').onclick = function()
{
    return confirm("Are you sure you want to delete this artist?");
};
</script>

<?php endif; ?>

<?php
include_once $phpAbs . "admin/templates/footer.php";
?>