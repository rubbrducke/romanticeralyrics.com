<?php


/*====================================================================
    
    DOCUMENT HEADER

====================================================================*/

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "admin/templates/header.php";


/*====================================================================

    POST: EntryCommand

====================================================================*/

if( isset( $_POST['entryCommand'] ) )
{
    switch( $_POST['entryCommand'] )
    {
        case 'delete':
            $deleteStatement = $db->prepare( "DELETE FROM Score WHERE id=:id" );
            $deleteStatement->bindParam( ':id', $_POST['id'] );
            $deleteStatement->execute();
            break;
    }

    $user->redirect($htmlAbs . "admin/dashboard.php");

}


/*====================================================================
    
    POST: Everything else

====================================================================*/

if( isset( $_POST['id'] ) && ! isset( $_POST['entryCommand'] ) )
{


    /*--------------------------------------------------------------------
        Establish a common refID
    --------------------------------------------------------------------*/

    $refID = $_POST['refID'];

    if( $refID == '' )
    {
        $scoreTitle = explode( ' ', $_POST['title'] );
        $lengths = array_map( 'strlen', $scoreTitle );
        $longestWord = max($lengths);
        $index = array_search( $longestWord, $lengths );

        $scoreTitle = $scoreTitle[ $index ];
        $composerName = array_slice( explode( ' ', $_POST['composer'] ), -1 )[0];

        $refID = $composerName == '' ? $scoreTitle : $composerName . '_' . $scoreTitle;
    }


    /*====================================================================
        File upload
    ====================================================================*/

    if( ! isset($_POST['fromCollection']) 
        && isset( $_FILES['sheets']['size'] ) 
        && $_FILES['sheets']['size'][0] > 0 )
    {
        $destination = $phpAbs . "score_images/individual/$refID";
        uploadImages( $_FILES['sheets']['tmp_name'], $refID, $destination );

    }

    
    /*====================================================================
        Rename files
    ====================================================================*/

    if( isset( $_POST['images'] ) )
    {

        $images = $_POST['images'];

        $firstImage = array_slice( $images, 0, 1 );

        $filepath = $firstImage[0]['filename'];
        $filepath = str_replace( $htmlAbs, $phpAbs, $filepath );
        
        $collection = preg_replace( '/[0-9]*.jpg/', '', basename( $filepath ) );

        $jpegFolderPath = dirname( $filepath ) . '/';
        $thumbsFolderPath = dirname( dirname( $filepath ) ) . '/thumbs/';

        $tempPrefix = 'temp_';

        foreach( $images as $image )
        {

            /*--------------------------------------------------------------------
                Information Processing
            --------------------------------------------------------------------*/

            $filepath = $image['filename'];
            $filepath = str_replace( $htmlAbs, $phpAbs, $filepath );

            $filename = basename( $filepath );

            $oldNumber = preg_replace( '/[^0-9]*/', '', $filename );
            $newNumber = sprintf( '%03d', $image['order'] );

            if( $image['delete'] == 1 )
            {
                unlink( $jpegFolderPath . $filename );
                unlink( $thumbsFolderPath . str_replace('.jpg', '_t.jpg', $filename ) );
                continue;

            }


            /*--------------------------------------------------------------------
                Update the filename if it has changed
            --------------------------------------------------------------------*/

            if( $oldNumber != $newNumber )
            {
                $oldJPEG = $filepath;
                $oldThumb = $thumbsFolderPath . $collection . $oldNumber . '_t.jpg';

                $newJPEG = $jpegFolderPath . $tempPrefix . $collection . $newNumber . '.jpg';
                $newThumb = $thumbsFolderPath . $tempPrefix . $collection . $newNumber . '_t.jpg';

                rename( $oldJPEG, $newJPEG );
                rename( $oldThumb, $newThumb );

            }

        }


        /*--------------------------------------------------------------------
            Remove 'temp_' from the filenames
        --------------------------------------------------------------------*/

        $jpegs = glob( "$jpegFolderPath/*$tempPrefix*[0-9].jpg" );
        foreach( $jpegs as $jpeg )
        {
            $newName = str_replace( $tempPrefix, '', $jpeg );
            rename( $jpeg, $newName );
            
        }

        $thumbs = glob( "$thumbsFolderPath/*temp_*[0-9]_t.jpg" );
        foreach( $thumbs as $thumb )
        {
            $newName = str_replace( $tempPrefix, '', $thumb );
            rename( $thumb, $newName );

        }

    }


    /*====================================================================
        Work
    ====================================================================*/

    $workCheck = $db->prepare( "SELECT id FROM Work WHERE title=:title LIMIT 1" );
    $workCheck->bindParam( ':title', $_POST['work'] );
    $workCheck->execute();

    if( $workCheck->rowCount() > 0 )
    {
        $work = $workCheck->fetch(PDO::FETCH_ASSOC);

        $workID = $work['id'];
    }

    else
    {
        $workStatement = $db->prepare( "INSERT INTO Work ( title ) VALUES ( :title )" );
        
        if( $_POST['work'] == '' )
        {
            $workStatement->bindParam( ':title', $_POST['title'] );
        
        } // if
        
        else
        {
            $workStatement->bindParam( ':title', $_POST['work'] );
        }

        $workStatement->execute();
        $workID = $db->lastInsertId();
    }


    /*====================================================================
        Score
    ====================================================================*/

    if( $_POST['id'] == 'new' )
    {
        $scoreStatement = $db->prepare( "INSERT INTO Score ( work_id, ref_id, title, year, dedicatee, form, key_sig, time_sig, tempo, instruments, cover_desc, note, description, source, metadata_author, page_count ) VALUES ( :workID, :refID, :title, :year, :dedicatee, :form, :keySig, :timeSig, :tempo, :instruments, :coverDescription, :note, :description, :source, :metadataAuthor, :pageCount )" );
    } // if

    else
    {
        $scoreStatement = $db->prepare( "UPDATE Score SET work_id=:workID, ref_id=:refID, title=:title, year=:year, dedicatee=:dedicatee, form=:form, key_sig=:keySig, time_sig=:timeSig, tempo=:tempo, instruments=:instruments, cover_desc=:coverDescription, note=:note, description=:description, source=:source, metadata_author=:metadataAuthor, page_count=:pageCount WHERE id=:scoreID" );

        $scoreStatement->bindParam( ':scoreID', $_POST['id'] );

    } // else

    if( $_POST['fromCollection'] == '1' )
    {
        $scoreStatement->bindParam( ':refID', NULL );
    }

    else
    {
        $scoreStatement->bindParam( ':refID', $refID );
    }


    $year = $_POST['year'] != '' && $_POST['year'] != '0' ? (int) $_POST['year'] : null;

    $scoreStatement->bindParam( ':workID', $workID );
    $scoreStatement->bindParam( ':title', $_POST['title'] );
    $scoreStatement->bindValue( ':year', $year, PDO::PARAM_INT );
    $scoreStatement->bindParam( ':dedicatee', $_POST['dedicatee'] );
    $scoreStatement->bindParam( ':form', $_POST['form'] );
    $scoreStatement->bindParam( ':keySig', $_POST['keySig'] );
    $scoreStatement->bindParam( ':timeSig', $_POST['timeSig'] );
    $scoreStatement->bindParam( ':tempo', $_POST['tempo'] );
    $scoreStatement->bindParam( ':instruments', $_POST['instruments'] );
    $scoreStatement->bindParam( ':coverDescription', $_POST['coverDescription'] );
    $scoreStatement->bindParam( ':note', $_POST['note'] );
    $scoreStatement->bindParam( ':description', $_POST['description'] );
    $scoreStatement->bindParam( ':source', $_POST['source'] );
    $scoreStatement->bindParam( ':metadataAuthor', $_POST['metadataAuthor'] );
    $scoreStatement->bindParam( ':pageCount', $_POST['pageCount'] );

    $scoreStatement->execute();

    $db->lastInsertId();

    $scoreID = $_POST['id'] == 'new' ? $db->lastInsertId() : $_POST['id'];

    /*====================================================================
        Augmented Notes
    ====================================================================*/

    if( $_POST['fromCollection'] == '1' )
    {
        $fileDestination = $phpAbs . "/score_images/$refID/audio/";
    
    }

    else
    {
        $fileDestination = $phpAbs . "/score_images/individual/$refID/audio/";

    }

    if( ! is_dir( $fileDestination ) )
    {
        mkdir( $fileDestination, 0777 , true );
    }

    if( $_FILES['mp3']['size'] > 0 )
    {
        $mp3Filepath = $fileDestination . basename( $_FILES['mp3']['name'] );
        $mp3Filetype = pathinfo( $mp3Filepath, PATHINFO_EXTENSION );

        $mp3RelativePath = str_replace( $phpAbs, '', $mp3Filepath );

        $mp3Uploaded = move_uploaded_file( $_FILES['mp3']['tmp_name'], $mp3Filepath );

    } // if

    if( $_FILES['ogg']['size'] > 0 )
    {
        $oggFilepath = $fileDestination . basename( $_FILES['ogg']['name'] );
        $oggFiletype = pathinfo( $oggFilepath, PATHINFO_EXTENSION );

        $oggRelativePath = str_replace( $phpAbs, '', $oggFilepath );
        
        $oggUploaded = move_uploaded_file( $_FILES['ogg']['tmp_name'], $oggFilepath );

    } // if

    $augnotesCheck = $db->prepare( "SELECT id FROM Augnotes WHERE score_id=:scoreID" );
    $augnotesCheck->bindParam( ':scoreID', $scoreID );
    $augnotesCheck->execute();

    if( $augnotesCheck->rowCount() > 0 )
    {
        $augnote = $augnotesCheck->fetch(PDO::FETCH_ASSOC);
        $augID = $augnote['id'];

        $augnotesStatement = $db->prepare( "UPDATE Augnotes SET score_id=:scoreID, data=:data, mp3_path=:mp3, ogg_path=:ogg, performers=:performers, date=:date, info=:info WHERE id=:augID" );
        $augnotesStatement->bindParam( ':augID', $augID );
    
    } // if

    elseif( $_POST['data'] != '' || isset( $_FILES['mp3'] ) || isset( $_FILES['ogg'] ) )
    {
        $augnotesStatement = $db->prepare( "INSERT INTO Augnotes ( score_id, data, mp3_path, ogg_path, performers, date, info ) VALUES ( :scoreID, :data, :mp3, :ogg, :performers, :date, :info )" );

    } // else

    if( $augnotesCheck->rowCount() > 0 || $_FILES['mp3']['size'] > 0 || $_FILES['ogg']['size'] > 0 )
    {
        $augnotesStatement->bindParam( ':scoreID', $scoreID );
        $augnotesStatement->bindParam( ':data', $_POST['data'] );
        
        $mp3Data = isset($mp3Uploaded) ? $mp3RelativePath : '';
        $augnotesStatement->bindParam( ':mp3', $mp3Data );

        $oggData = isset($oggUploaded) ? $oggRelativePath : '';
        $augnotesStatement->bindParam( ':ogg', $oggData );



        $augnotesStatement->bindParam( ':performers', $_POST['performers'] );
        $augnotesStatement->bindParam( ':date', $_POST['date'] );
        $augnotesStatement->bindParam( ':info', $_POST['info'] );

        $augnotesStatement->execute();

    }


    /*====================================================================
        Poet
    ====================================================================*/

    /*--------------------------------------------------------------------
        Insert New Data
    --------------------------------------------------------------------*/

    if( $_POST['composer'] != '' )
    {

        // Smash up the name 
        
        $name = explode( ' ', $_POST['composer'] );

        $first = '';
        $middle = '';
        $last = $name[0];
        
        if( sizeof($name) > 1 )
        {
            $first = $name[0];
            $last = $name[ sizeof($name)-1 ];
        }

        if( sizeof($name) > 2 )
        {
            $middle = array_slice( $name, 1, sizeof($name) - 2 );
            $middle = implode( ' ', $middle );
        }

        // Prepare and run existence check

        $firstComp = $first == '' ? '(f_name IS NULL OR f_name LIKE :first)' : 'f_name LIKE :first';
        $middleComp = $middle == '' ? '(m_name IS NULL OR m_name LIKE :middle)' : 'm_name LIKE :middle';
        $lastComp = $last == '' ? '(l_name IS NULL OR l_name LIKE :last)' : 'l_name LIKE :last';

        $composerCheck = $db->prepare( "SELECT id FROM Artist WHERE $firstComp AND $middleComp AND $lastComp" );
        $composerCheck->bindParam( ':first', $first );
        $composerCheck->bindParam( ':middle', $middle );
        $composerCheck->bindParam( ':last', $last );

        $composerCheck->execute();

        // Artist Exists
        if( $composerCheck->rowCount() > 0 )
        {
            $composer = $composerCheck->fetch(PDO::FETCH_ASSOC);
            $composerID = $composer['id'];
        
        } // if

        // Artist Does Not Exist
        else
        {

            $composerInsert = $db->prepare( "INSERT INTO Artist ( f_name, m_name, l_name ) VALUES ( :first, :middle, :last )" );
            $composerInsert->bindParam( ':first', $first );
            $composerInsert->bindParam( ':middle', $middle );
            $composerInsert->bindParam( ':last', $last );

            $composerInsert->execute();

            $composerID = $db->lastInsertId();

        } // else

        $composerScoreCheck = $db->prepare( "SELECT artist_id FROM Artist_has_Score WHERE score_id=:scoreID" );
        $composerScoreCheck->bindParam( ':scoreID', $workID );
        $composerScoreCheck->execute();

        // Some Composer-Score Relationship Exists
        if( $composerScoreCheck->rowCount() > 0 )
        {
            $composer = $composerScoreCheck->fetch(PDO::FETCH_ASSOC);

            // Composer-Score Relationship is not the same as it is now
            if( $composer['artist_id'] != $composerID )
            {
                $composerStatement = $db->prepare( "UPDATE Artist_has_Score SET artist_id=:artistID WHERE score_id=:scoreID" );
                $composerStatement->bindParam( ':artistID', $composerID );
                $composerStatement->bindParam( ':scoreID', $scoreID );

                $composerStatement->execute();

            } // if

        } // if

        // Composer-Score Relationship Does Not Exist
        else
        {
            $composerStatement = $db->prepare( "INSERT INTO Artist_has_Score ( artist_id, score_id ) VALUES ( :artistID, :scoreID )" );
            $composerStatement->bindParam( ':artistID', $composerID );
            $composerStatement->bindParam( ':scoreID', $scoreID );

            $composerStatement->execute();
        
        } // else

    } // if


    /*====================================================================
        Publisher
    ====================================================================*/


    /*--------------------------------------------------------------------
        Purge Old Data
    --------------------------------------------------------------------*/

    // Going to need the Publisher id related to this score

    $pubStatement = $db->prepare( "SELECT Publisher_id FROM Publisher_has_Score WHERE Score_id=:scoreID" );
    $pubStatement->bindParam( ':scoreID', $scoreID );
    $pubStatement->execute();

    $publisherID = $pubStatement->fetch(PDO::FETCH_ASSOC);
    $publisherID = $publisherID['Publisher_id'];

    // Remove this Score from publisher records
    // It shouldn't exist anywhere in this table so delete without regard to the publisher id

    $purgeStatement = $db->prepare( "DELETE FROM Publisher_has_Score WHERE Score_id=:scoreID" );
    $purgeStatement->bindParam( ':scoreID', $scoreID );
    $purgeStatement->execute();

    // Delete the Publisher if they have no other data

    $purgeStatement = $db->prepare( 
        "DELETE FROM Publisher 
        WHERE 
        id=:pubID
        AND
        ( SELECT COUNT(pS.Publisher_id) FROM Publisher_has_Score pS WHERE pS.Publisher_id=:pubID ) = 0
        AND
        ( SELECT COUNT(pC.Publisher_id) FROM Publisher_has_Collection pC WHERE pC.Publisher_id=:pubID ) = 0" );

    $purgeStatement->bindParam( ':pubID', $publisherID );
    $purgeStatement->execute();


    /*--------------------------------------------------------------------
        Insert New Data
    --------------------------------------------------------------------*/

    if( $_POST['publisher'] != '' )
    {
        $publisherCheck = $db->prepare( "SELECT id FROM Publisher WHERE name=:publisher LIMIT 1" );
        $publisherCheck->bindParam( ':publisher', $_POST['publisher'] );
        $publisherCheck->execute();

        // Publisher Exists

        if( $publisherCheck->rowCount() > 0 )
        {
            $publisher = $publisherCheck->fetch(PDO::FETCH_ASSOC);
            $publisherID = $publisher['id'];
        
        } // if

        // Publisher Does Not Exist

        else
        {
            $publisherInsert = $db->prepare( "INSERT INTO Publisher ( name ) VALUES ( :publisher )");
            $publisherInsert->bindParam( ':publisher', $_POST['publisher'] );
            $publisherInsert->execute();

            $publisherID = $db->lastInsertId();
        
        } // else

        $publisherScoreCheck = $db->prepare( "SELECT publisher_id FROM Publisher_has_Score WHERE score_id=:scoreID" );
        $publisherScoreCheck->bindParam( ':scoreID', $scoreID );
        $publisherScoreCheck->execute();

        // Publisher-Score Relationship Exists

        if( $publisherScoreCheck->rowCount() > 0 )
        {
            $publisher = $publisherScoreCheck->fetch(PDO::FETCH_ASSOC);

            // Publisher-Score Relationship is not the same as it is now

            if( $publisher['publisher_id'] != $publisherID )
            {
                $publisherStatement = $db->prepare( "UPDATE Publisher_has_Score SET publisher_id=:publisherID WHERE score_id=:scoreID" );
                $publisherStatement->bindParam( ':publisherID', $publisherID );
                $publisherStatement->bindParam( ':scoreID', $scoreID );

                $publisherStatement->execute();
            
            } // if
        
        } // if

        // Publisher-Score Relationship Does Not Exist

        else
        {
            $publisherStatement = $db->prepare( "INSERT INTO Publisher_has_Score ( publisher_id, score_id ) VALUES ( :publisherID, :scoreID )" );
            $publisherStatement->bindParam( ':publisherID', $publisherID );
            $publisherStatement->bindParam( ':scoreID', $scoreID );

            $publisherStatement->execute();
        
        } // else

    } //if

    // $scoreID is defined during Score insertions/deletions
    $user->redirect($htmlAbs . "admin/editor/score.php?id=$scoreID");
    
} // if POST['id']


/*====================================================================
    
    GET

====================================================================*/

if( isset( $_GET['id']) )
{
    $id = $_GET['id'];


    /*====================================================================
        Score
    ====================================================================*/

    $dbStatement = $db->prepare( "SELECT * FROM Score WHERE id=:id LIMIT 1" );
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $results = $dbStatement->fetch(PDO::FETCH_ASSOC);


    /*====================================================================
        Augmented Notes
    ====================================================================*/

    $dbStatement = $db->prepare( "SELECT * FROM Augnotes WHERE score_id=:id" );
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $augnotes = $dbStatement->fetch(PDO::FETCH_ASSOC);

    $mp3Filename = basename( $augnotes['mp3_path'] );
    $oggFilename = basename( $augnotes['ogg_path'] );


    /*====================================================================
        Work
    ====================================================================*/

    $dbStatement = $db->prepare( "SELECT w.title, w.id FROM Work w INNER JOIN Score s ON s.work_id=w.id AND s.id=:id" );
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $work = $dbStatement->fetch(PDO::FETCH_ASSOC);


    /*====================================================================
        Composer
    ====================================================================*/

    $dbStatement = $db->prepare( "SELECT a.f_name, a.m_name, a.l_name, a.id FROM Artist a INNER JOIN Artist_has_Score aScore ON a.id=aScore.artist_id AND aScore.score_id=:id" );
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $composer = $dbStatement->fetch(PDO::FETCH_ASSOC);


    /*====================================================================
        Publisher
    ====================================================================*/

    $dbStatement = $db->prepare( "SELECT p.name, p.id FROM Publisher p INNER JOIN Publisher_has_Score pS ON p.id=pS.publisher_id AND pS.score_id=:id" );
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $publisher = $dbStatement->fetch(PDO::FETCH_ASSOC);
}

?>


<!--==================================================================
    
    SCORE FORM

===================================================================-->

<form class="data-entry" method="post" enctype="multipart/form-data">


    <!--==================================================================
        Form Submission
    ===================================================================-->

    <?php if( ( $_GET['id'] == 'new' && $user->canCreate() ) || $user->canModify() ) : ?>

    <button class='primary' type="submit">Save</button>

    <?php endif; ?>


    <!--==================================================================
        Basic Information
    ===================================================================-->

    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ID
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <?php
    $augID = $augnotes['id'];

    $refID = isset($_GET['ref']) ? $_GET['ref'] : $results['ref_id'];
    $fromCollection = isset($_GET['ref']) ? 1 : 0;

    echo "<input type='hidden' name='fromCollection' value='$fromCollection' />";
    echo "<input type='hidden' name='id' value='$id' />";
    echo "<input type='hidden' name='refID' value='$refID' />";
    echo "<input type='hidden' name='augID' value='$augID' />";
    ?>

    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Work
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    
    <h2>Work</h2>
    

    <?php if( $_GET['id'] != 'new' ) : ?>

    <div class='linkedInput'>

        <input type='text' name='work' list='works' value="<?= $work['title']; ?>" />
        
        <a target='_blank' href="<?=$htmlAbs?>admin/editor/work.php?id=<?= $work['id'] ?>" class='score-link'>
            <img src="<?=$htmlAbs?>admin/editor/images/documentIcon.png">
        </a>

    </div>

    <?php else : ?>

    <input type='text' name='work' list='works' value="<?= $work['title']; ?>" />

    <?php endif; ?>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Works List
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <datalist id='works'>
    <?php
        $dbStatement = $db->prepare( "SELECT title FROM Work ORDER BY title ASC" );
        $dbStatement->execute();

        $works = $dbStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach( $works as $work )
        {
            $title = $work['title'];
            echo "<option value='$title'>";
        }
    ?>
    </datalist>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Title
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Title</h2>
    <input type="text" name="title" value="<?= $results['title'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Composer
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Composer</h2>
    <p>Do not include titles or suffixes.</p>
    <?php
    $composerName = trim( $composer['f_name'] . ' ' . $composer['m_name'] . ' ' . $composer['l_name'] );
    $composerName = preg_replace( '/ +/', ' ', $composerName );
    ?>

    <?php if( $_GET['id'] != 'new' ) : ?>

    <div class='linkedInput'>

        <input type='text' name="composer" list='artists' value="<?= $composerName ?>" />
        
        <a target='_blank' href="<?=$htmlAbs?>admin/editor/work.php?id=<?= $composer['id'] ?>" class='score-link'>
            <img src="<?=$htmlAbs?>admin/editor/images/documentIcon.png">
        </a>

    </div>

    <?php else : ?>

    <input type='text' name="composer" list='artists' value="<?= $composerName ?>" />

    <?php endif; ?>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Composer List
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <datalist id='artists'>
    <?php
        $dbStatement = $db->prepare( "SELECT f_name, m_name, l_name FROM Artist ORDER BY f_name ASC, l_name ASC" );
        $dbStatement->execute();

        $artists = $dbStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach( $artists as $artist )
        {
            $name = trim( $artist['f_name'] . ' ' . $artist['m_name'] . ' ' . $artist['l_name'] );
            $name = preg_replace( '/ +/', ' ', $name );
            echo "<option value='$name'>";
        }
    ?>
    </datalist>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Publisher
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Publisher</h2>
    
    <?php if( $_GET['id'] != 'new' ) : ?>

        <div class='linkedInput'>

            <input type='text' name='publisher' list='publishers' value='<?= $publisher['name'] ?>' />
            
            <a target='_blank' href="<?=$htmlAbs?>admin/editor/publisher.php?id=<?= $publisher['id'] ?>" class='score-link'>
                <img src="<?=$htmlAbs?>admin/editor/images/documentIcon.png">
            </a>

        </div>

    <?php else : ?>

    <input type='text' name='publisher' list='publishers' value='<?= $publisher['name'] ?>' />

    <?php endif; ?>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Publisher List
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <datalist id='publishers'>
    <?php
        $dbStatement = $db->prepare( "SELECT name FROM Publisher ORDER BY name ASC" );
        $dbStatement->execute();

        $publishers = $dbStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach( $publishers as $publisher )
        {
            $name = $publisher['name'];
            echo "<option value='$name'>";
        }
    ?>
    </datalist>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Year
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Year</h2>
    <input type="text" name="year" maxlength='4' value="<?= $results['year'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Description
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Description</h2>
    <textarea rows='4' cols='50' name="description"><?= $results['description'] ?></textarea>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Description
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Cover Description</h2>
    <textarea rows='4' cols='50' name="cover-description"><?= $results['cover_desc'] ?></textarea>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Dedicatee
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Dedicatee</h2>
    <input type="text" name="dedicatee" value="<?= $results['dedicatee'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Instruments
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Instruments</h2>
    <input type="text" name="instruments" value="<?= $results['instruments'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Key Signature
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Key Signature</h2>
    <input type="text" name="key-signature" value="<?= $results['key_sig'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Time Signature
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Time Signature</h2>
    <input type="text" name="time-signature" value="<?= $results['time_sig'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Form
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Form</h2>
    <input type="text" name="form" value="<?= $results['form'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Tempo
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Tempo</h2>
    <input type="text" name="tempo" value="<?= $results['tempo'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Additional Notes
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Additional Notes</h2>
    <textarea rows='4' cols='50' name="note"><?= $results['note'] ?></textarea>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Metadata Authors
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Metadata Authors</h2>
    <input type="text" name="metadata-author" value='<?= $results['metadata_author'] ?>' />


    <!--==================================================================
        Augmented Notes
    ===================================================================-->

    <h2>Augmented Notes</h2>
    <fieldset>


        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            Data
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <h3>Data</h3>
        <textarea rows='15' cols='50' name="data"><?= $augnotes['data'] ?></textarea>


        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            MP3
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <h3>MP3 Audio</h3>

        <?php if( $augnotes['mp3_path'] != '' ) : ?>

            <label>Existing File: </label><?= basename( $augnotes['mp3_path'] ) ?>
        
        <?php endif; ?>

        <input type="file" id='mp3' name='mp3'>


        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            OGG
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <h3>OGG Audio</h3>

        <?php if( $augnotes['ogg_path'] != '' ) : ?>
            <label>Existing File: </label><?= basename( $augnotes['ogg_path'] ) ?>
        <?php endif; ?>

        <input type="file" id='ogg' name='ogg'>


        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            Performers
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <h3>Performers</h3>
        <input type="text" name="performers" value="<?= $augnotes['performers'] ?>" />


        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            Information
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <h3>Information</h3>
        <textarea rows='4' cols='50' name="info"><?= $augnotes['info'] ?></textarea>


        <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            Recording Date
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

        <?php
            $year = ''; 
            $month = ''; 
            $day = '';

            $date = explode( '-', $augnotes['date'] );
            if( sizeof( $date ) == 3 )
            {
                $year = $date[0];
                $month = $date[1];
                $day = $date[2];
            }
        ?>

        <h3>Date of Recording</h3>
        <fieldset class="date">
            <input type="text" name="month" placeholder="MM" maxlength="2" value='<?= $month ?>' />
            <input type="text" name="day" placeholder="DD" maxlength="2" value='<?= $day ?>' />
            <input type="text" name="year" placeholder="YYYY" maxlength="4" value='<?= $year ?>' />
        </fieldset>
    </fieldset>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Score Images
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Score Images</h2>

    <?php if( !$fromCollection && $user->canUpload() ) : ?>
        <label>Select one or more photos</label>
        <input type='file' name='sheets[]' multiple />
    <?php endif; ?>


<!--==================================================================
   
    SCORE IMAGES

===================================================================-->

<div class='data-entry gallery'>
<?php
if( isset( $_GET['id'] ) && $_GET['id'] != 'new' ) :

    $dbStatement = $db->prepare( "SELECT s.ref_id, s.page_count FROM Score s WHERE s.id=:scoreID" );
    $dbStatement->bindParam( ':scoreID', $id );
    $dbStatement->execute();

    $result = $dbStatement->fetch(PDO::FETCH_ASSOC);

    if( $dbStatement->rowCount() > 0 ) :
    ?>

        <ul class='images sortable'>

        <?php
        if( $fromCollection ) :

            $refID = $_GET['ref'];
            $startIndex = $_GET['start'] - 1;
            $endIndex = $startIndex + ($result['page_count'] - 1);

            $lowResSearch = $phpAbs . "score_images/$refID/thumbs/*[0-9]_t.jpg";
            $highResSearch = $phpAbs . "score_images/$refID/jpeg/*[0-9].jpg";

            $lowResImages = glob( $lowResSearch );
            $highResImages = glob( $highResSearch );

            $arrayCounter = 0;
            for( $i = $startIndex; $i <= $endIndex; $i++ ) :

                $lowResImage = str_replace( $phpAbs, $htmlAbs, $lowResImages[$i] );
                $highResImage = str_replace( $phpAbs, $htmlAbs, $highResImages[$i] );

            ?>

                <li href='<?= $highResImage . '?' . date_timestamp_get( date_create() ) ?>'>
                    
                    <!-- Thumbnail -->

                    <img class='collection-image' src='<?= $lowResImage . '?' . date_timestamp_get( date_create() ) ?>' />
                    
                </li>

            <?php
            endfor;

        else :

            $refID = $result['ref_id'];
            $lowResSearch = $phpAbs . "score_images/individual/$refID/thumbs/*[0-9]_t.jpg";
            $highResSearch = $phpAbs . "score_images/individual/$refID/jpeg/*[0-9].jpg";

            $lowResImages = glob( $lowResSearch );
            $highResImages = glob( $highResSearch );

            foreach( $lowResImages as $index => $lowResImage ) :

                $lowResImage = str_replace( $phpAbs, $htmlAbs, $lowResImage );
                $highResImage = str_replace( $phpAbs, $htmlAbs, $highResImages[$index] );
            ?>

                <li href='<?= $highResImage . '?' . date_timestamp_get( date_create() ) ?>'>
            
                    <!-- Thumbnail -->

                    <img class='score-image' src='<?= $lowResImage . '?' . date_timestamp_get( date_create() ) ?>' />
                    
                    <!-- Filename -->

                    <input type='hidden' name='images[<?= $index + 1 ?>][filename]' value='<?= $highResImage ?>' />
                    
                    <!-- Order -->

                    <input type='hidden' name='images[<?= $index + 1 ?>][order]' value='<?= $index + 1 ?>' />

                    <!-- Delete -->

                    <input type='hidden' name='images[<?= $index + 1 ?>][delete]' value='0' />
                    <div class='close' onclick='delete(this)'></div>
                    
                </li>

            <?php 
            endforeach; // foreach image

        endif; ?>

        </ul>

    <?php else : ?>

        <div class='flag-empty'>None</div>
    
    <?php endif; ?>

<?php endif; ?>

</div>

</form>

<?php if( ! isset($_GET['ref'] ) ) : ?>

<?php if( $user->canDelete() ) : ?>

<!--==================================================================
    
    DELETION FORM

===================================================================-->

<form id='delete-form' method='post'>
    <input type='hidden' name='id' value='<?= $id ?>' />
    <button class='caution' type='submit' name='entryCommand' value='delete'>Delete</button>
</form>

<?php endif; ?>

<!--==================================================================
    
    SCRIPTS

===================================================================-->

<script>
document.getElementById('delete-form').onclick = function()
{
    return confirm("Deleting this score will also delete the images and augmented notes data. \nAre you sure you want to delete this Score?");
};
</script>

<script src="<?=$htmlAbs?>admin/libs/jquery-1.7.1.min.js"></script>
<script src="<?=$htmlAbs?>admin/libs/jquery-ui.min.js"></script>
<script src="<?=$htmlAbs?>admin/libs/jquery.magnific-popup.min.js"></script>
<script>
window.onload = function()
{
    if( window.jQuery )
    {

        $( '.close' ).click( function(e){
            e.stopImmediatePropagation();
            $(this).prev('input').val('1').closest('li').hide();
        });

        $( '.sortable.images li' ).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });

        $( ".sortable" ).sortable( 
        {
            stop: function( e, ui )
            {
                counter = 0;
                $(this).children().each( function( index, value )
                {
                    if( $(value).find( 'input:nth-of-type(3)' ).val() != '1' )
                    {
                        $(value).find( 'input:nth-of-type(2)' ).val( counter + 1 );
                        counter++;

                    }

                } );

            } // stop

        }); // $('.sortable')

    } // if( window.jQuery )

} // window.onload

</script>

<?php endif; ?>

<?php
include_once $phpAbs . "admin/templates/footer.php";
?>