<?php
$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "admin/templates/header.php";


/*====================================================================

    POST: EntryCommand

====================================================================*/

if( isset( $_POST['entryCommand'] ) )
{
    switch( $_POST['entryCommand'] )
    {
        case 'delete':
            $deleteStatement = $db->prepare( "DELETE FROM Work WHERE id=:id" );
            $deleteStatement->bindParam( ':id', $_POST['id'] );
            $deleteStatement->execute();
            break;
    }

    $user->redirect($htmlAbs . "admin/dashboard.php");

}


/*====================================================================
    
    POST

====================================================================*/

if( isset( $_POST['id'] ) )
{
    if( $_POST['id'] == 'new' )
    {
        $dbStatement = $db->prepare( "INSERT INTO Work ( title, year ) VALUES ( :title, :year )" );
    
    } // if

    else
    {
        $dbStatement = $db->prepare( "UPDATE Work SET title=:title, year=:year WHERE id=:id" );
        
        $id = $_POST['id'];
        $dbStatement->bindParam( 'id', $id );
    
    } // else

    $dbStatement->bindParam( 'title', $_POST['title'] );
    $dbStatement->bindParam( 'year', $_POST['year'] );

    $dbStatement->execute();

    $workID = $_POST['id'] == 'new' ? $db->lastInsertId() : $_POST['id'];

    if( $_POST['poet'] )
    {
        // Smash up the name 

        $name = explode( ' ', $_POST['poet'] );

        $first = '';
        $middle = '';
        $last = $name[0];

        if( sizeof($name) > 1 )
        {
            $first = $name[0];
            $last = $name[ sizeof($name) - 1 ];
        }

        if( sizeof($name) > 2 )
        {
            $middle = array_slice( $name, 1, sizeof($name) - 2 );
            $middle = implode( ' ', $middle );
        }

        // Prepare and run existence check

        $firstComp = $first == '' ? '(f_name IS NULL OR f_name LIKE :first)' : 'f_name LIKE :first';
        $middleComp = $middle == '' ? '(m_name IS NULL OR m_name LIKE :middle)' : 'm_name LIKE :middle';
        $lastComp = $last == '' ? '(l_name IS NULL OR l_name LIKE :last)' : 'l_name LIKE :last';

        $poetCheck = $db->prepare( "SELECT id FROM Artist WHERE $firstComp AND $middleComp AND $lastComp" );
        $poetCheck->bindParam( ':first', $first );
        $poetCheck->bindParam( ':middle', $middle );
        $poetCheck->bindParam( ':last', $last );

        $poetCheck->execute();


        // Artist Exists
        if( $poetCheck->rowCount() > 0 )
        {
            $poet = $poetCheck->fetch(PDO::FETCH_ASSOC);
            $poetID = $poet['id'];
        
        } // if

        // Artist Does Not Exist 
        else
        {
            $poetInsert = $db->prepare( "INSERT INTO Artist ( f_name, m_name, l_name ) VALUES ( :first, :middle, :last )" );
            $poetInsert->bindParam( ':first', $first );
            $poetInsert->bindParam( ':middle', $middle );
            $poetInsert->bindParam( ':last', $last );

            $poetInsert->execute();

            $poetID = $db->lastInsertId();
        }

        $poetWorkCheck = $db->prepare( "SELECT artist_id FROM Artist_has_Work WHERE work_id=:workID" );
        $poetWorkCheck->bindParam( ':workID', $workID );
        $poetWorkCheck->execute();

        // Some Poet-Work Relationship Exists
        if( $poetWorkCheck->rowCount() > 0 )
        {
            $poet = $poetWorkCheck->fetch(PDO::FETCH_ASSOC);

            // Poet-Work Relationship is not the same as it is now
            if( $poet['artist_id'] != $poetID )
            {
                $poetStatement = $db->prepare( "UPDATE Artist_has_Work SET artist_id=:artistID WHERE work_id=:workID" );
                $poetStatement->bindParam( ':artistID', $poetID );
                $poetStatement->bindParam( ':workID', $workID );

                $poetStatement->execute();

            } // if

        } // if

        // Poet-Work Relationship Does Not Exist
        else
        {
            $poetStatement = $db->prepare( "INSERT INTO Artist_has_Work ( artist_id, work_id ) VALUES ( :artistID, :workID )" );
            $poetStatement->bindParam( ':artistID', $poetID );
            $poetStatement->bindParam( ':workID', $workID );

            $poetStatement->execute();
        
        } // else

    } // if


    /*====================================================================
        Redirect if newly inserted artist
    ====================================================================*/

    if( $_POST['id'] == 'new' )
    {
        $id = $db->getInsertId();
        $user->redirect( $htmlAbs . "admin/editor/artist.php?id=$workID" );
    
    }

} // if $_POST


/*====================================================================

    GET

====================================================================*/

if( isset( $_GET['id']) )
{
    $id = $_GET['id'];

    /*====================================================================
        Work
    ====================================================================*/

    $dbStatement = $db->prepare("SELECT * FROM Work WHERE id=:id LIMIT 1");
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $results = $dbStatement->fetch(PDO::FETCH_ASSOC);

    /*====================================================================
        Poet
    ====================================================================*/

    $dbStatement = $db->prepare( "SELECT a.f_name, a.m_name, a.l_name, a.id FROM Artist a INNER JOIN Artist_has_Work aWork ON a.id=aWork.artist_id AND aWork.work_id=:id" );
    $dbStatement->bindParam( ':id', $id );
    $dbStatement->execute();

    $poet = $dbStatement->fetch(PDO::FETCH_ASSOC);
}

?>

<!--==================================================================
    FORM
===================================================================-->

<form class="data-entry" method="post">


    <!--==================================================================
        Form Submission
    ===================================================================-->

    <?php if( ( $_GET['id'] == 'new' && $user->canCreate() ) || $user->canModify() ) : ?>

    <button class='primary' type="submit">Save</button>

    <?php endif; ?>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ID
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <input type='hidden' name='id' value="<?= $id ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Title
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Title</h2>
    <input type="text" name="title" value="<?= $results['title'] ?>" />


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Poet
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Poet</h2>
    <?php 
    $poetName = trim( $poet['f_name'] . ' ' . $poet['m_name'] . ' ' . $poet['l_name'] );
    $poetName = preg_replace( '/ +/', ' ', $poetName );
    ?>

    <?php if( $_GET['id'] != 'new' ) : ?>

    <div class='linkedInput'>
        <input type='text' name='poet' list='artists' value="<?= $poetName ?>" />
        <a target='_blank' href="<?=$htmlAbs?>admin/editor/artist.php?id=<?= $poet['id'] ?>" class='score-link'>
            <img src="<?=$htmlAbs?>admin/editor/images/documentIcon.png">
        </a>
    </div>

    <?php else : ?>

    <input type='text' name='poet' list='artists' value="<?= $poetName ?>" />

    <?php endif; ?>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Poet List
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <datalist id='artists'>
    <?php
        $dbStatement = $db->prepare( "SELECT f_name, m_name, l_name FROM Artist ORDER BY f_name ASC, l_name ASC" );
        $dbStatement->execute();

        $artists = $dbStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach( $artists as $artist )
        {
            $name = trim( $artist['f_name'] . ' ' . $artist['m_name'] . ' ' . $artist['l_name'] );
            $name = preg_replace( '/ +/', ' ', $name );
            echo "<option value='$name'>";
        }
    ?>
    </datalist>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Year
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <h2>Year</h2>
    <input type="text" name="year" value="<?= $results['year'] ?>" />


    <!--==================================================================
        H2
    ===================================================================-->

    <h2>Scores</h2>
    <div class='gallery'>
    <?php
    if( isset( $_GET['id'] ) && $_GET != 'new' )
    {
        $dbStatement = $db->prepare( "SELECT s.title, s.ref_id, s.id FROM Score s INNER JOIN Work ON s.work_id=Work.id AND Work.id=:workID" );
        $dbStatement->bindParam( ':workID', $id );
        $dbStatement->execute();

        $results = $dbStatement->fetchAll(PDO::FETCH_ASSOC);

        if( $dbStatement->rowCount() > 0 )
        {
            foreach( $results as $result )
            {
                if( $result['ref_id'] != '' )
                {
                    $refID = $result['ref_id'];
                    $workID = $result['id'];
                    $scoreTitle = $result['title'];

                    $image = getDirectoryThumbnail( "score_images/individual/$refID/thumbs/" );
                    generateHTMLThumbLink( $scoreTitle, $htmlAbs . "admin/editor/score.php?id=$workID", $image );
                }
            }
        }

        else
        {
            echo "<div class='flag-empty'>None</div>";
        }
    }
    ?>
    </div>

</form>

<?php if( $user->canDelete() ) : ?>

<!--==================================================================
    
    DELETION FORM

===================================================================-->

<form id='delete-form' method='post'>
    <input type='hidden' name='id' value='<?= $id ?>' />
    <button class='caution' type='submit' name='entryCommand' value='delete'>Delete</button>
</form>


<!--==================================================================
    
    SCRIPTS

===================================================================-->

<script>
document.getElementById('delete-form').onclick = function()
{
    return confirm("Deleting this Work will also delete the Scores within it. \nAre you sure you want to delete this Work?");
};
</script>

<?php endif; ?>

<?php
include_once $phpAbs . "admin/templates/footer.php";
?>