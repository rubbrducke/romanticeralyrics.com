<?php


/*====================================================================

    DOCUMENT HEADER

====================================================================*/

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "conf/database.php";
include_once $phpAbs . "admin/class.user.php";
include_once $phpAbs . "admin/editor/editorFunctions.php";

$user = new user( $db );


/*====================================================================
    Basic User Redirects
====================================================================*/


/*--------------------------------------------------------------------
    Redirect all users that aren't logged in to the login page
--------------------------------------------------------------------*/

if( !$user->is_logged_in() && !isset($onLoginPage) )
{
    $user->redirect( $htmlAbs . "admin/index.php" );
}


/*--------------------------------------------------------------------
    Redirect logged in users from the login page to the dashboard
--------------------------------------------------------------------*/

else if( $user->is_logged_in() && isset($onLoginPage) )
{
    $user->redirect( $htmlAbs . "admin/dashboard.php" );
}

else if( ! $user->is_logged_in() || ! $user->isAdmin() )
{
    $user->redirect( $htmlAbs . "admin/dashboard.php" );
}

?>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1" />

<link rel="stylesheet" type="text/css" href="<?= $htmlAbs . "admin/style.css" ?>">

<title>Dashboard</title>
</head>

<body>

<?php

if( isset( $_POST['users'] ) )
{
    $contentEditors = $_POST['users'];

    foreach( $contentEditors as $editor )
    {

        // Create new user
        if( isset( $editor['newUser'] ) && $editor['delete'] == '0' )
        {
            if( $editor['email'] == '' || $editor['email'] == '@' )
            {
                continue;
            
            }

            $contentEditorstatement = $db->prepare( "INSERT INTO Users ( userID, userEmail, userPass, secQuestion, secAnswer, creation, modification, fileUpload, deletion ) VALUES ( :id, :email, :pass, :secQ, :secA, :creation, :modification, :fileUpload, :deletion )" );

            $defaultPassword = password_hash( 'password', PASSWORD_DEFAULT );
            $securityQuestion = 'You should really create a security question. Now enter the word okay';
            $securityAnswer = password_hash( 'okay', PASSWORD_DEFAULT );

            $create = isset( $editor['Create'] ) ? 1 : 0;
            $modify = isset( $editor['Modify'] ) ? 1 : 0;
            $upload = isset( $editor['File Upload'] ) ? 1 : 0;
            $delete = isset( $editor['Delete'] ) ? 1 : 0;

            $contentEditorstatement->bindParam( ':id', $editor['id'] );
            $contentEditorstatement->bindParam( ':email', $editor['email'] );
            $contentEditorstatement->bindParam( ':pass', $defaultPassword );
            $contentEditorstatement->bindParam( ':secQ', $securityQuestion );
            $contentEditorstatement->bindParam( ':secA', $securityAnswer );
            $contentEditorstatement->bindParam( ':creation', $create );
            $contentEditorstatement->bindParam( ':modification', $modify );
            $contentEditorstatement->bindParam( ':fileUpload', $upload );
            $contentEditorstatement->bindParam( ':deletion', $delete );

            $contentEditorstatement->execute();
        }

        // Delete existing user
        else if( ! isset( $editor['newUser'] ) && $editor['delete'] == '1' )
        {
            $contentEditorstatement = $db->prepare( "DELETE FROM Users WHERE userID=:id AND admin=0" );
            $contentEditorstatement->bindParam( ':id', $editor['id'] );

            $contentEditorstatement->execute();
        }

        // Not a new user, not requesting deletion. Must be an update!
        else if ( ! isset( $editor['newUser'] ) )
        {
            $updateUserStatement = $db->prepare( "UPDATE Users SET creation=:creation, modification=:modification, fileUpload=:fileUpload, deletion=:deletion WHERE userID=:id AND admin=0" );

            $create = isset( $editor['Create'] ) ? 1 : 0;
            $modify = isset( $editor['Modify'] ) ? 1 : 0;
            $upload = isset( $editor['File Upload'] ) ? 1 : 0;
            $delete = isset( $editor['Delete'] ) ? 1 : 0;

            $updateUserStatement->bindParam( ':id', $editor['id'] );
            $updateUserStatement->bindParam( ':creation', $create );
            $updateUserStatement->bindParam( ':modification', $modify );
            $updateUserStatement->bindParam( ':fileUpload', $upload );
            $updateUserStatement->bindParam( ':deletion', $delete );

            $updateUserStatement->execute();

        }

    }

}

?>

<style>
    input 
    {
        width:              auto;
    }

    label
    {
        padding-right:      20px;
    }

    #users
    {
        margin:             10px auto;
        width:              60%;
    }
</style>

<div id='users'>

    <form class='userForm' method='post'>

        <button type='button' onclick='createUser(this.parentElement)' style='width: 100%;' />New User</button>

        <?php
        $dbStatement = $db->prepare("SELECT * FROM Users WHERE admin=0 ORDER BY userEmail ASC");
        $dbStatement->execute();

        $contentEditors = $dbStatement->fetchAll(PDO::FETCH_ASSOC);

        $editorIdStatement = $db->prepare( "SELECT MAX(userID) as 'maxID' FROM Users" );
        $editorIdStatement->execute();

        $maxUserID = $editorIdStatement->fetch(PDO::FETCH_ASSOC);        
        $maxUserID = $editorIdStatement->rowCount() > 0 ? $maxUserID['maxID'] : 0;

        foreach( $contentEditors as $key => $editor ) :

        $permissions = array(   'Create' => $editor['creation'], 
                                'Modify' => $editor['modification'], 
                                'File Upload' => $editor['fileUpload'], 
                                'Delete' => $editor['deletion'] 
                            );
        ?>

            <div class='user'>

                <input type='hidden' name='users[<?= $key ?>][id]' value='<?= $editor['userID'] ?>' />
                <input type='hidden' class='deleteUser' name='users[<?= $key ?>][delete]' value='0' />

                <input type='button' onclick="remove(this)" value='X' />
                <input type='email' name='users[<?= $key ?>][email]' placeholder='Email' value='<?= $editor['userEmail'] ?>' readonly />

                <div class='permissions'>

                    <label>Permissions: </label>

                    <?php if( ! in_array( '0', $permissions, true ) ) : ?>

                        <div class='checkbox'>
                            <input type='checkbox' onclick="checkAll(this)" id='<?= $key ?>-All' checked />
                            <label for='<?= $key ?>-All'>All</label>
                        </div>

                    <?php else : ?>

                        <div class='checkbox'>
                            <input type='checkbox' onclick="checkAll(this)" id="<?= $key ?>-All" />
                            <label for='<?= $key ?>-All'>All</label>
                        </div>

                    <?php 
                    endif;

                    foreach( $permissions as $permission => $value ) : 
                        $checked = $value != 0 ? 'checked' : '';
                    ?>

                        <div class='checkbox'>
                            <input type='checkbox' id='<?= $key ?>-<?= $permission ?>' name='users[<?= $key ?>][<?= $permission ?>]' value='1' <?= $checked ?> />
                            <label for='<?= $key ?>-<?= $permission ?>'><?= $permission ?></label>
                        </div>

                    <?php endforeach; ?>

                </div>

            </div>

        <?php endforeach; ?>

        <button type='submit' class='primary'>Save</button>

        <div class='alert success' style='display: none'>
            <span class='closebtn' onclick="this.parentElement.style.display='none';">&times;</span>
            Success!
        </div>
        <div class='alert failure' style='display: none'>
            <span class='closebtn' onclick="this.parentElement.style.display='none';">&times;</span>
            Error posting data.
        </div>

    </form>

</div>

<script src="<?=$htmlAbs?>admin/libs/jquery-1.7.1.min.js"></script>
<script>

    function checkAll( input )
    {
        $(input).parent().siblings( 'div.checkbox' ).children( 'input' ).prop( 'checked', $(input).prop('checked') );
    }

    var maxDBId = <?= $maxUserID ?>;

    function createUser( form )
    {
        maxDBId++;
        var userID = maxDBId;

        var inputAll = $( "<input type='checkbox' onclick='checkAll(this)' id='" + userID + "'>" );
        var labelAll = $( "<label for='" + userID + "'> All</label>" );

        var inputCreate = $( "<input type='checkbox' name='users[" + userID + "][Create]' id='" + userID + "-All' value='1'>" );
        var labelCreate = $( "<label for='" + userID + "-All'> Create</label>" );

        var inputModify = $( "<input type='checkbox' name='users[" + userID + "][Modify]' id='" + userID + "-Modify' value='1'>" );
        var labelModify = $( "<label for='" + userID + "-Modify'> Modify</label>" );

        var inputUpload = $( "<input type='checkbox' name='users[" + userID + "][File Upload]' id='" + userID + "-Upload' value='1'>" );
        var labelUpload = $( "<label for='" + userID + "-Upload'> File Upload</label>" );

        var inputDelete = $( "<input type='checkbox' name='users[" + userID + "][Delete]' id='" + userID + "-Delete' value='1'>" );
        var labelDelete = $( "<label for='" + userID + "-Delete'> Delete</label>" );

        var divAll = $( "<div class='checkbox'></div>").append( $(inputAll) ).append( $(labelAll) );

        var divCreate = $( "<div class='checkbox'></div>").append( $(inputCreate) ).append( $(labelCreate) );

        var divModify = $( "<div class='checkbox'></div>").append( $(inputModify) ).append( $(labelModify) );
        
        var divUpload = $( "<div class='checkbox'></div>").append( $(inputUpload) ).append( $(labelUpload) );

        var divDelete = $( "<div class='checkbox'></div>").append( $(inputDelete) ).append( $(labelDelete) );

        var permissionsElem = $( "<div class='permissions'></div>" )
            .append( "<label>Permissions: </label>")
            .append( $(divAll) )
            .append( $(divCreate) )
            .append( $(divModify) )
            .append( $(divUpload) )
            .append( $(divDelete) );

        $( "<div class='user'></div>" ).insertAfter( $('.userForm button').first() )
            .append( " <input type='hidden' name='users[" + userID + "][newUser]' value='1' >" )
            .append( " <input type='hidden' name='users[" + userID + "][id]' value='" + userID + "' value='" + userID + "' />" )
            .append( " <input type='hidden' class='deleteUser' name='users[" + userID + "][delete]' value='0' />" )
            .append( " <input type='button' onclick='remove(this)' value='X' />" )
            .append( " <input type='email' name='users[" + userID + "][email]' placeholder='Email' />" )
            .append( $(permissionsElem) );
    }

    function remove( input )
    {
        $(input).siblings( 'input.deleteUser' ).val('1');
        $(input).parent().css('display', 'none');
    }

</script>