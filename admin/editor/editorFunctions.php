<?php

function getDirectoryThumbnail( $directory, $index = 1 )
{
    $phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
    $htmlAbs = getenv( 'APP_ROOT_PATH' );

    $searchDir = $phpAbs . $directory;
    $images = glob( "$searchDir/*.jpg" );
    sort($images);

    if( sizeof($images) > 0 && sizeof($images) >= $index )
    {
        $image = $images[($index-1)];
        $image = str_replace( $phpAbs, $htmlAbs, $image );
        return $image;
    }
}

function generateHTMLThumbLink( $title, $linkPath, $imagePath )
{
    echo "<a href='$linkPath'>";
    echo "<h3>$title</h3>";
    echo "<img src='$imagePath'>";
    echo "</a>";
}

function deleteImage( $imageURL )
{
    $phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
    $htmlAbs = getenv( 'APP_ROOT_PATH' );

    $fullPath = str_replace( $htmlAbs, $phpAbs, $imageURL );
    $realPath = realpath( $fullPath );

    $deletedImage = basename( $realPath );

    $dir = dirname( $realPath ) . '/';
    $search = $dir . '*[0-9].jpg';
    $images = array_map( 'basename', glob( $search ) );

    $deletedImageIndex = in_array( $deletedImage, $images ) ? array_search( $deletedImage, $images ) : sizeof($images);

    if( is_writeable( $realPath ) )
    {
        unlink( $realPath );

    } // if

    for( $i = $deletedImageIndex+1; $i < sizeof($images); $i++ )
    {
        $image = $images[$i];

        $filename = basename( $image );
        $fileNumber = preg_match( '/[0-9][0-9][0-9]/', $image, $matches ) ? $matches[0] : -1 ;

        $newFileNumber = sprintf( "%03d", ($fileNumber-1) );
        $newFilename = str_replace( $fileNumber, $newFileNumber, $filename );

        $oldFilename = $dir . $filename;
        $newFilename = $dir . $newFilename;

        rename( $oldFilename, $newFilename );

    }

}

function swapImage( $imageURL, $direction )
{

    if( $direction == 'left' )
    {
        $indexIncrement = -1;
    }

    else if( $direction == 'right' )
    {
        $indexIncrement = 1;
    }

    $phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
    $htmlAbs = getenv( 'APP_ROOT_PATH' );

    $fullPath = str_replace( $htmlAbs, $phpAbs, $imageURL );
    $realPath = realpath( $fullPath );
    $dir = dirname($realPath) . '/';

    $needle = basename($realPath);

    $search = $dir . '*[0-9].jpg';
    $haystack = array_map( 'basename', glob( $search ) );

    $index = in_array( $needle, $haystack ) ? array_search( $needle, $haystack ) : -1;

    // Return if:
    //  Going left and index is left-most item
    //  Going right and index is right-most item
    if( ( $direction == 'left' && $index == 0 ) || ( $direction == 'right' && $index == sizeof($haystack)-1 ) )
    {
        return;
    }

    if( $index > -1 && $index < sizeof($haystack) )
    {
        $indexNameA = $index + $indexIncrement;
        $indexNameB = $index;

        $oldNameA = $haystack[ $indexNameA ];
        $oldNameB = $haystack[ $indexNameB ];

        // Swap name numbers
        $newNumberA = preg_match( '/[0-9][0-9][0-9]/', $oldNameB, $matches ) ? $matches[0] : -1 ;
        $newNumberB = preg_match( '/[0-9][0-9][0-9]/', $oldNameA, $matches ) ? $matches[0] : -1 ;

        // Run replacements
        $tempNameA = $dir . preg_replace( '/[0-9][0-9][0-9]/', $newNumberA . '_temp', $oldNameA );
        $newNameA = $dir . preg_replace( '/[0-9][0-9][0-9]/', $newNumberA, $oldNameA );
        $newNameB = $dir . preg_replace( '/[0-9][0-9][0-9]/', $newNumberB, $oldNameB );

        $oldNameA = $dir . $oldNameA;
        $oldNameB = $dir . $oldNameB;

        // A -> TempA
        rename( $oldNameA, $tempNameA );

        // B -> NewB
        rename( $oldNameB, $newNameB );

        // A -> NewA
        rename( $tempNameA, $newNameA );
    }
}

function uploadImages( $images, $refID, $destination )
{

    $backupsFolder = "$destination/jpeg_backups/";
    $displayFolder = "$destination/jpeg/";
    $thumbsFolder = "$destination/thumbs/";

    realpath( $backupsFolder );
    realpath( $displayFolder );
    realpath( $thumbsFolder );

    if( !is_dir($backupsFolder) )
    {
        mkdir($backupsFolder, 0777, true);
    
    } // if

    if( !is_dir($displayFolder) )
    {
        mkdir($displayFolder, 0777, true);
    
    } // if

    if( !is_dir($thumbsFolder) )
    {
        mkdir($thumbsFolder, 0777, true);
    
    } // if

    $backupFiles = new FilesystemIterator( $backupsFolder, FilesystemIterator::SKIP_DOTS );
    $displayFiles = new FilesystemIterator( $displayFolder, FilesystemIterator::SKIP_DOTS );
    $thumbFiles = new FilesystemIterator( $thumbsFolder, FilesystemIterator::SKIP_DOTS );

    $numDispFiles = iterator_count( $displayFiles );
    $displayFolderIndex = $numDispFiles > 0 ? ($numDispFiles+1) : 1;

    $numThumbFiles = iterator_count( $thumbFiles );
    $thumbsFolderIndex = $numThumbFiles > 0 ? ($numThumbFiles+1) : 1;
    
    foreach( $images as $image )
    {

        /*====================================================================
            Create backups
        ====================================================================*/

        $previouslyUploaded = false;

        foreach( $backupFiles as $file )
        {
            if( identical($file, $image) )
            {
                $previouslyUploaded = true;
                break;
            
            } // if
        
        } // foreach

        if( $previouslyUploaded == false )
        {
            $numBackupImages = iterator_count( $backupFiles );
            $backupsIndex = $numBackupImages > 0 ? ($numBackupImages+1) : 1;

            $fullFilepath = $backupsFolder . $refID . sprintf( '%03d', $backupsIndex ) . '.jpg';
            copy( $image, $fullFilepath );
        
        } // if

        
        $originalImage = imagecreatefromjpeg( $image );

        /*====================================================================
            Create display image
        ====================================================================*/

        list($width, $height) = getimagesize($image);
        $newWidth = 1024;
        $newHeight = floor( ($newWidth/$width) * $height );

        $displayImage = imagecreatetruecolor( $newWidth, $newHeight );
        imagecopyresampled( $displayImage, $originalImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height );

        $fullFilepath = $displayFolder . $refID . sprintf( '%03d', $displayFolderIndex ) . '.jpg';
        $success = imagejpeg( $displayImage, $fullFilepath );

        /*====================================================================
            Create thumbnails
        ====================================================================*/

        list($width, $height) = getimagesize($image);
        $newWidth = 200;
        $newHeight = floor( ($newWidth/$width) * $height );

        $thumb = imagecreatetruecolor( $newWidth, $newHeight );
        imagecopyresampled( $thumb, $originalImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height );

        $fullFilepath = $thumbsFolder . $refID . sprintf( '%03d', $thumbsFolderIndex ) . '_t.jpg';
        imagejpeg( $thumb, $fullFilepath );
    
        $displayFolderIndex++;
        $thumbsFolderIndex++;

    } // foreach

} // function

function identical($fileOne, $fileTwo)
{
    if (filetype($fileOne) !== filetype($fileTwo)) return false;
    if (filesize($fileOne) !== filesize($fileTwo)) return false;
 
    if (! $fp1 = fopen($fileOne, 'rb')) return false;
 
    if (! $fp2 = fopen($fileTwo, 'rb'))
    {
        fclose($fp1);
        return false;
    }
 
    $same = true;
 
    while (! feof($fp1) and ! feof($fp2))
        if (fread($fp1, 4096) !== fread($fp2, 4096))
        {
            $same = false;
            break;
        }
 
    if (feof($fp1) !== feof($fp2)) $same = false;
 
    fclose($fp1);
    fclose($fp2);
 
    return $same;
}

?>