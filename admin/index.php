<?php

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

$onLoginPage = true;
include_once $phpAbs . "admin/templates/header.php";


$userIP = $_SERVER['REMOTE_ADDR'];

// for testing on localhost
if( $userIP == '' ){ $userIP = '1'; }

if( isset( $_POST['button-login']))
{
    $userEmail = trim($_POST['user-email']);
    $userEmail = filter_var( $userEmail, FILTER_SANITIZE_EMAIL );

    $userPass = trim($_POST['user-pass']);
    $userPass = strip_tags( $userPass );

    if( $user->login($userEmail, $userPass, $userIP) )
    {
        if( $user->checkKnownIP( $userEmail, $userIP ) )
        {
            $user->redirect('dashboard.php');
        }

        else
        {
            $check = $user->checkKnownIP( $userEmail, $userIP );
            $requestQA = 'Please answer the following security question.';
        }
    }
    
    else
    {
        $error = 'Incorrect username or password.';
    }
    
} // if button-login


if( isset( $_POST['button-QA']) )
{
    $pass = $user->checkSecurityQuestion( $_POST['user-email'], $userIP, $_POST['security-question'], $_POST['security-answer'] );

    if( $pass == TRUE )
    {
        $user->redirect('dashboard.php');
    }
    
    else
    {
        $user->logout();
        $user->redirect('index.php');
    }

} // if button-QA

?>

<form id="login" method="post">

    <?php
    if( isset($error) ) :
    ?>
        <div class="alert">
               <p><?=$error?></p>
        </div>
    <?php
    endif;
    ?>

    <?php
    if( !isset($requestQA) ) :
    ?>
        <h2>Sign in</h2>
        <input type="text" name="user-email" placeholder="Email Address" required />
        <input type="text" name="user-pass" placeholder="Password" />

        <button type="submit" class='primary' name="button-login">Sign In</button>

    <?php
    
    else :
    ?>
        <h2><?=$requestQA?></h2>
        <input type="hidden" name="user-email" value=<?=$_POST['user-email']?> />
        <input type="text" name="security-question" value="<?php echo $user->getSecurityQuestion( $userEmail ); ?>" readonly />
        <input type="text" name="security-answer" required />

        <button type="submit" class='primary' name="button-QA">Submit</button>
    <?php
    endif;
    ?>

</form>

<?php
include_once $phpAbs . "admin/templates/footer.php";
?>