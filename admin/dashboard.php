<?php 


/*====================================================================

    DOCUMENT HEADER

====================================================================*/

$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "admin/templates/header.php";


/*====================================================================

    GET

====================================================================*/

$activeButton = '';

if( isset($_GET['archive']))
{


    /*====================================================================
        Pull data from the appropriate table
    ====================================================================*/

    $activeButton = $_GET['archive'];

    switch( $activeButton )
    {


        /*--------------------------------------------------------------------
            Artists
        --------------------------------------------------------------------*/

        case 'artist':
            $table = 'artist';

            $itemStatement = $db->prepare("SELECT id, f_name, l_name FROM Artist ORDER BY f_name ASC, l_name ASC");
            $itemStatement->execute();

            $results = $itemStatement->fetchAll(PDO::FETCH_ASSOC);
            break;


        /*--------------------------------------------------------------------
            Publishers
        --------------------------------------------------------------------*/

        case 'publisher':
            $table = 'publisher';

            $itemStatement = $db->prepare("SELECT id, name FROM Publisher ORDER BY name ASC");
            $itemStatement->execute();

            $results = $itemStatement->fetchAll(PDO::FETCH_ASSOC);
            break;


        /*--------------------------------------------------------------------
            Scores
        --------------------------------------------------------------------*/

        case 'score':
            $table = 'score';

            $itemStatement = $db->prepare(
                "SELECT 
                    s.id, 
                    s.title, 
                    '<h6>Composer: ', a.f_name, 
                    ' ', a.m_name, 
                    ' ', a.l_name, 
                    '</h6>',
                    CONCAT( '<h6>Publisher: ', p.name, '</h6>' )
                    
                FROM Score s 

                LEFT JOIN Artist_has_Score aScore 
                    ON aScore.score_id = s.id 

                LEFT JOIN Artist a 
                    ON a.id = aScore.artist_id 

                LEFT JOIN Publisher_has_Score publisherScore
                    ON publisherScore.Score_id = s.id

                LEFT JOIN Publisher p 
                    ON p.id=publisherScore.Publisher_id

                WHERE s.id NOT IN (SELECT score_id FROM Collection_has_Score)");

            $itemStatement->execute();

            $results = $itemStatement->fetchAll(PDO::FETCH_ASSOC);
            break;


        /*--------------------------------------------------------------------
            Works
        --------------------------------------------------------------------*/

        case 'work':
            $table = 'work';

            $itemStatement = $db->prepare(
                "SELECT w.id, w.title 
                FROM Work w 
                INNER JOIN Score s 
                    ON w.id=s.work_id AND s.id NOT IN (SELECT score_id FROM Collection_has_Score) 
                GROUP BY w.id 
                ORDER BY title ASC");

            $itemStatement->execute();

            $results = $itemStatement->fetchAll(PDO::FETCH_ASSOC);
            break;


        /*--------------------------------------------------------------------
            Collections
        --------------------------------------------------------------------*/

        case 'collection':
            $table = 'collection';

            $itemStatement = $db->prepare("SELECT id, title FROM Collection ORDER BY title ASC");
            $itemStatement->execute();

            $results = $itemStatement->fetchAll(PDO::FETCH_ASSOC);
            break;
    }
}

?>


<!--==================================================================
    Dashboard Navigation
===================================================================-->

<form id="nav-dashboard" method="get">

    <button type="submit" name="archive" value="artist">Artists</button>

    <button type="submit" name="archive" value="publisher">Publishers</button>

    <button type="submit" name="archive" value="score">Individual Scores</button>

    <button type="submit" name="archive" value="work">Works</button>

    <button type="submit" name="archive" value="collection">Collections</button>

    <?php if( $user->isAdmin() ) : ?>

    <button type="submit" name="accounts" value="1">Users</button>

    <?php endif; ?>

</form>

<?php


/*====================================================================
    Generate the HTML to display the table contents
====================================================================*/

if( isset( $_GET['archive']) )
{


    /*--------------------------------------------------------------------
        Alphabet Navigation
    --------------------------------------------------------------------*/

    echo "<div id='nav-alphabet'>";

    foreach( range('A', 'Z') as $letter )
    {
        echo "<a href='#$letter' class='scroll'>$letter</a>";
    }

    echo "</div>";


    /*--------------------------------------------------------------------
        Table Contents
    --------------------------------------------------------------------*/

    echo "<div id='items'>";

    if( $user->canCreate() )
    {
        echo "<a style='width: 100%; text-align: center; text-transform: uppercase;' href='" . $htmlAbs . "admin/editor/$table.php?id=new'>New Entry</a>";
    }

    if( isset($results) )
    {
        $entries = array();
        
        foreach( $results as $row )
        {
            $linkName = '';
            $keys = array_keys($row);

            for( $col = 1; $col < count($row); $col++ )
            {
                $linkName = $linkName . $row[ $keys[$col] ] . ' ';
            }
            
            $linkName = trim( preg_replace( '/^(the|a|an){1} ([^<\n\r]*) /i', '\2, \1', $linkName ) );
            $entries[] = array( 'id' => $row['id'], 'name' => $linkName );

        }
        
        /*--------------------------------------------------------------------
            Sort the things!
        --------------------------------------------------------------------*/

        usort( $entries, function($a, $b){
            return strcasecmp( $a['name'], $b['name'] );
        });

        $currentLetter = '';

        foreach( $entries as $entry )
        {
            if( strcasecmp( $currentLetter, substr($entry['name'], 0, 1) ) < 0 )
            {
                if( $currentLetter != '' )
                    echo "</div>";

                $currentLetter = substr($entry['name'], 0, 1);
                echo "<div id='$currentLetter'>";
                echo "<div style='display: block; margin: 20px 1%;'>$currentLetter</div>";
                
            }

            echo "<a href='" . $htmlAbs . "admin/editor/$table.php?id=" . $entry['id'] . "'>";
            echo $entry['name'];
            echo '</a>';
        }
    }

}

else if( isset($_GET['accounts']) )
{
    echo "<iframe style='width: 100%; height: 50%;' src='" . $htmlAbs . "admin/editor/viewUsers.php'></iframe>";
}

else
{
    echo "<div class='abs-centered'>Click on one of the tabs to browse the database contents!</div>";
}

?>

</div>

<script src="<?=$htmlAbs?>admin/libs/jquery-1.7.1.min.js"></script>
<script>
    jQuery(document).ready(function($) 
    {
        $(".scroll").click(function(event) 
        {
            event.preventDefault();
            $('html,body').animate( { scrollTop:$(this.hash).offset().top } , 1000); 
        });
    });

    var buttons = document.querySelectorAll('#nav-dashboard button');
    [].forEach.call( buttons, function(button){
        if( button.value == "<?=$activeButton?>" )
        {
            button.classList.add('active');
        }
    });
</script>

<?php include_once $phpAbs . "admin/templates/footer.php"; ?>