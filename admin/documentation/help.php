<?php
$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
$htmlAbs = getenv( 'APP_ROOT_PATH' );

include_once $phpAbs . "admin/templates/header.php";
?>

<div style='margin: 50px auto; width: 60%;'>

<center><h1>Welcome to your backend management interface!</h1></center>

<h2 id='login'>Login</h2>

<p>Users must log in with an email and password. If they are logging in from an unknown computer they will be required to answer a security question to log in.</p>

<?php if( $user->isAdmin() ) : ?>

    <br>
    <p>The default admin email is: feldmanp@mailbox.sc.edu</p>
    <p>The default password is: romanticeralyrics</p>

<?php endif; ?>

<h2 id='dashboard'>Dashboard</h2>

<p>The dashboard is your backend's homepage. From here you can access artist, publisher, score, work, collection, and user records.</p>

<h2 id='account'>Account</h2>

<p>On the Account page you can update your email address, password, and/or security question. You have to verify yourself using your current login information in order for the update to process.</p>

<h2 id='artist'>Artist, Publisher, and Work Pages</h2>

<p>On these pages you can enter general information and see which Scores, Works, and Collections they are associated with.</p>

<center>
    <img style='max-width: 400px; margin: 50px 10px 10px 10px;' src='images/Link.gif'/>
    <h4 style='max-width: 400px; margin: 10px;'>The page icons link to associated records.</h4>
</center>

<center>
    <img style='max-width: 400px; margin: 50px 10px 10px 10px;' src='images/Pre-Filled Names.gif'/>
    <h4 style='max-width: 400px; margin: 10px;'>Some fields have autofill enabled to minimize duplicate entries and misspelling.</h4>
</center>

<h2 id='score'>Score</h2>

<p>On the Score page you can enter general information, augmented notes data, and upload audio files and sheet music images.</p>
<br>
<p>If a Score belongs to a Collection then you can not move or delete its images.</p>

<center>
    <img style='max-width: 400px; margin: 50px 10px 10px 10px;' src='images/Image Zoom.gif'/>
    <h4 style='max-width: 400px; margin: 10px;'>You can zoom in on an image by clicking on it.</h4>
</center>

<center>
    <img style='max-width: 400px; margin: 50px 10px 10px 10px;' src='images/Score - Move.gif'/>
    <h4 style='max-width: 400px; margin: 10px;'>You can rearrange the sheet order by clicking and dragging images.</h4>
</center>

<center>
    <img style='max-width: 400px; margin: 50px 10px 10px 10px;' src='images/Score - Deletion.gif'/>
    <h4 style='max-width: 400px; margin: 10px;'>You can delete an image by clicking the giant 'X'.</h4>
</center>

<h2 id='collection'>Collection</h2>

<p>On the Collection page you can enter general information, upload sheet music images, and modify the scores associated with those images.</p>

<h3>Gallery</h3>

<center>
    <img style='max-width: 400px; margin: 50px 10px 10px 10px;' src='images/Collection - Two-Page.gif'/>
    <h4 style='max-width: 400px; margin: 10px;'>You can extend brackets down to span a score across multiple pages.</h4>
</center>

<center>
    <img style='max-width: 400px; margin: 50px 10px 10px 10px;' src='images/Collection - Multipage.gif'/>
    <h4 style='max-width: 400px; margin: 10px;'>You can continue to extend a bracket across multiple pages.</h4>
</center>

<center>
    <img style='max-width: 400px; margin: 50px 10px 10px 10px;' src='images/Collection - Rearrange.gif'/>
    <h4 style='max-width: 400px; margin: 10px;'>You can rearrange the sheet order by clicking and dragging images.</h4>
</center>

</div>
<?php
include_once $phpAbs . "admin/templates/footer.php";
?>