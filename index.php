<!DOCTYPE html>
<html lang="en" ng-app="romantic_era_lyrics">

<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">

	<title>Romantic-Era Lyrics</title><!-- Typekit -->

	<script src="//use.typekit.net/izf8zmo.js" type="text/javascript"></script>
	<script type="text/javascript">
try{Typekit.load();}catch(e){}
	</script><!-- Bootstrap -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style2.css" rel="stylesheet">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-route.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-sanitize.js"></script>
	<script src="js/plugins/dirPagination.js"></script>

	<script src="appjs/real.js"></script>
</head>

<body>
	<section class="prologue" data-offsety="-200" data-speed="6" data-type="background" id="first" ng-controller="index_search_controller as search">
		<div class="container">
			<div class="row" style="margin-top:5%">
				<div data-speed="-2" data-type="sprite" id="logo"></div>
			</div>

			<div class="row">
				<div class="col-md-12" style="text-align: center;">
					<ul>
						<li>
							<a href="search.html">Advanced Search</a>
						</li>

						<li>
							<a href="browse.html">Browse</a>
						</li>

						<li>
							<a href="about.php">About the Project</a>
						</li>
					</ul>
				</div>
			</div>

<!-- 			<div class="row">
				<div class="col-md-12">
					<h1>Search the Romantic-Era Lyrics Database</h1>
				</div>
			</div> -->

			<div class="row">
				<form id="searchform" name="searchform" novalidate="" ng-submit="redirectSearch()">
					<div class="col-md-4">
						<select class="form-control input-lg" id="input_searchtype" ng-init="searchin='score'" ng-model="searchin">
							<option value="score">
								Search Scores
							</option>

							<option value="work">
								Search Works
							</option>

							<option value="artist">
								Search Artists
							</option>

							<option value="publisher">
								Search Publishers
							</option>

							<option value="collection">
								Search Collections
							</option>
						</select>
					</div>

					<div class="col-md-7 form-group">
						<label class="sr-only" for="exampleInputText">Keywords</label> <input class="form-control input-lg" id="exampleInputText" placeholder="Keywords" type="text" ng-model="searchstring">
					</div>

					<div class="col-md-1 form-group">
						<input class="btn btn-default btn-lg" type="submit" value="Search" ng-bind="search_redirect">
					</div>
				</form>
			</div>
		</div>

		<article></article>
	</section>

	<section id="search">
		<div class="container container-main"></div>
	</section>

	<section class="story" data-offsety="100" data-speed="4" data-type="background" id="second">
		<article>
			<div id="homeinfo">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<h1 class='editable'>Romantic-Era Poetry Set to Music</h1>
						</div>
					</div>

					<div class="row">
						<div class="col-md-3 col-md-offset-1">
							<h4 class='editable'>Poetry as its authors intended.</h4>
							<br>
							<a href="http://player.vimeo.com/video/140218993" data-toggle="lightbox">
								<div class="video">
									<img class="img-responsive" src="./img/video_thumb.png">
									<img class="video-play-button" src="./img/play_button.png">
								</div>
							</a>
						</div>

						<div class="col-md-4 editable">
							<?php 
							$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
							include( "$phpAbs/templates/index/second-column.txt" );
							?>
						</div>

						<div class="col-md-4 editable">
							<?php 
							$phpAbs = $_SERVER['DOCUMENT_ROOT'] . getenv( 'APP_ROOT_PATH' );
							include( "$phpAbs/templates/index/third-column.txt" );
							?>
						</div>
					</div>

					<div class="row spacer"></div>
				</div>
			</div>
		</article>
	</section>

	<div ng-include="'./templates/footer.html'"></div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins/ekko-lightbox.min.js"></script>
	<script type="text/javascript">
		$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox();
		}); 
	</script>
	<script src="js/retina-1.1.0.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/script.js"></script>
</body>
</html>