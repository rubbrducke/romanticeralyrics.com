/* real.js
 * Author: Victor Reynolds
 *
 * This is the file containing the controllers used by AngularJS to display the
 * data from the Romantic-Era Lyrics database API as dynamic html pages: the
 * "meat" of the frontend for our website.
 *
 * Global variables and functions are listed at the top of the file, followed
 * by a controller for each separate html page. One can see in the body of the
 * markup for each page where the controller is included. Almost all controllers
 * share the findData function, which is where the page pulls its respective
 * json data from the PHP API.
 *
 * Note: Due to design decisions, "lyricist" was changed to "poet" on the frontend
 * of the website, but in the backend we kept the word "lyricst" due to dependencies
 * on this term in the SQL database and in search page configurations. Of course these
 * can be changed, but it was easier to change the label than to change the convention.
*/
(function(){

/* Notice the included modules:
 * ngRoute - imported mainly to read id values from page URLs
 * ngSanitize - used to parse and display HTML for composer bios and publisher descriptions
 * dirPaginate - a very nice directive used to replace ng-repeat that automatically paginates data
*/
var app = angular.module('romantic_era_lyrics', ["ngRoute", "ngSanitize", "angularUtils.directives.dirPagination"]);
// Descriptions used in the browse and search pages for our categories of data.
// Note that performances are not a category in their own right, but are treated as such in the browse page.
var DATA_CATEGORY_DESCRIPTIONS = {
    score: "A score is an individual piece of sheet music. Scores are organized individually in this database, whether or not they were released individually.",
    performance: "Scores for which audio performances accompanied by Augmented Notes are available.",
    artist: "An artist can either be a poet or a composer. A poet is who wrote the words of an individual score, while a composer wrote the music. Many artists did both.",
    publisher: "The publisher of a score or collection of sheet music.",
    collection: "A collection is a publication that contains multiple individual scores.",
    work: 'A work is the literary work that a piece of sheet music is based on. This is how we group multiple versions of the same piece together. For example, there are multiple scores available for the work "Auld Robin Gray".'
};
// This seemed to be the best way to list categories to search by.
// This is a global variable because both the index page and the search page 
// need these values to search by.
var DATA_SEARCH_TYPES = {
    score: {title: true, composer: true, lyricist: true, publisher: true},
    artist: {f_name: true, l_name: true, biography: true},
    publisher: {name: true, city: true, description: true},
    collection: {title: true, publisher: true, score_titles: true},
    work: {title: true, lyricist: true, publisher_list: true, composer_list: true}
};

// A little helper function that scrolls the window to the top.
// This was used to scroll to the top of the page after new pages were selected
// in paginated data (such as in the search page).
function scrollToTop()
{
  $("html, body").animate({ scrollTop: 0 }, 400);
}

// This controller is used by the navigation header included in all of the data
// html pages to determine which page is active.
app.controller('nav_controller', function($scope, $location) {
    // This simply searches the url for the provided string, e.g.
    // the link for the score page will check isActive('score')
    // while the url containes /score.html, so it will be true in that case
    $scope.isActive = function(route) {
        return $location.absUrl().indexOf(route) > -1;
    }
});

// The controller for the score page.
// Note: This one is particularly complicated because I had to squash together two additional plugins
// in this page: Augmented Notes, and eSmartZoom.
// Also, the places where $timeout is used are instances where there are competing scripts
// accessing the same element, and we want our action here to be the final action.
app.controller("score_controller", function($scope, $routeParams, $location, $http, $timeout){
    var audio_elt;

    $scope.scoreInfo = [];
    $scope.scoreImageTab = 0;
    $scope.augnotesActive = false;

    function startAugmentedNotes()
    {
        if ($scope.scoreInfo.length !== 0)
        {
          if ($scope.scoreInfo["augnotes"].length !== 0)
          {
            // The page currently only supports one augmented notes display.
            $timeout(function() {
              window.augnotes = new AugmentedNotes($scope.scoreInfo["augnotes"][0]["data"]);
              var score_div = $(".score-div");
              audio_elt = $(".audtools audio");
              window.augnotes_ui = new AugmentedNotesUI(augnotes, score_div, audio_elt);
              audio_elt.on("timeupdate", function() {
                // Make sure that the appropriate tab is displayed.
                $scope.updateImageTab(window.augnotes_ui.currentMeasureID().page_num, false);
              });
              $scope.augnotesActive = true;
            }, 0);
          }
        }
    }

    function scoreHasAugmentedNotes()
    {
      return ($scope.scoreInfo.length !== 0 && $scope.scoreInfo["augnotes"].length !== 0);
    }

    function disableAugmentedNotes()
    {
      $(".audtools audio").trigger('pause');
      $timeout(function() {
        $scope.augnotesActive = false;
        $('.score-page .box').hide();
      }, 0);
    }
    
    function findData(parent){

        query = $location.search();
        $http({
            url: "/api/score.php", 
            method: "GET",
            params: {id: query.id}
        }).success(function(data, status, headers, config) {
            $scope.scoreInfo = data;

            if ( !scoreHasAugmentedNotes() )
            {
              $timeout(function() { $('#score_page_' + $scope.scoreImageTab).smartZoom(); }, 0);
              // Had to do this because smartZoom was breaking during window resizing.
              // It doesn't seem to be fully compatible with the max-width: 100% property
              // on the image.
              $( window ).resize(
                function() { 
                  $timeout(function() { $('#score_page_' + $scope.scoreImageTab).smartZoom("destroy"); }, 0);
                  $timeout(function() { $('#score_page_' + $scope.scoreImageTab).smartZoom(); }, 0);
                } 
              );
            }
            startAugmentedNotes();
        });
    };

    $scope.updateImageTab = function(tab, clicked) {
      // console.log("Tab: " + tab + "\nClicked:" + clicked);
      if (typeof(audio_elt) === "undefined" || audio_elt.get(0).paused === true || clicked === false)
      {
        $scope.scoreImageTab = tab;

        if (clicked === false)
        {
          $scope.$apply();
        }

        if (scoreHasAugmentedNotes() && $('#score_page_' + $scope.scoreImageTab).smartZoom('isPluginActive'))
        {
          // Had to disable smartzoom on page change for scores with Augmented notes
          // because of bizarre use cases, e.g. zooming in on page 2, switching to page 1,
          // hitting the play button will screw up the measure locations when page 2 comes back around
          $('#score_page_' + $scope.scoreImageTab).smartZoom('destroy');
          $('#score_page_' + $scope.scoreImageTab).removeAttr("style");
        }
        else if (!scoreHasAugmentedNotes())
        {
          $timeout(function() { $('#score_page_' + $scope.scoreImageTab).smartZoom(); }, 0);
        }
      }
    }

    // This function is used for the magnifying glass icon under the score image.
    $scope.zoom = function(factor) {
      if ( $scope.augnotesActive || !$('#score_page_' + $scope.scoreImageTab).smartZoom('isPluginActive')) {
        disableAugmentedNotes();
        $timeout( function () {
          $('#score_page_' + $scope.scoreImageTab).smartZoom();
          $('#score_page_' + $scope.scoreImageTab).smartZoom('zoom', factor);
        }, 0);
      }
      else
      {
        $('#score_page_' + $scope.scoreImageTab).smartZoom('zoom', factor);
      }
    }

    // This function is used to position the little arrow appropriately
    // for the thumbnail images dropdown. Timeout was used in case the user
    // decides to try to break the dropdown by clicking repeatedly.
    $scope.updateDropdownIcon = function(id) {
      var dropdown = $('#' + id);
      $timeout(function() {
        dropdown.removeClass('glyphicon-chevron-down glyphicon-chevron-right')
        if (dropdown.parent().hasClass('collapsed'))
        {
          dropdown.addClass('glyphicon-chevron-right');
        }
        else
        {
          dropdown.addClass('glyphicon-chevron-down');
        }
      }, 0.1);
    }

    // Used to reset the page without reloading the html if someone manually
    // changes the score id in the location bar of his or her browser.
    $scope.$on('$locationChangeStart', function(event) {
        findData(this);
    });
});

// The controller for the artist page. The artist page displays a biography and related scores/works
// for composers and poets. As is mentioned elsewhere, many of the people in our database who wrote
// poetry also wrote the music for it, so making composers and lyricists/poets separate categories
// would not make sense, unless we wanted to have duplicate entries in each table.
app.controller("artist_controller", function($scope, $routeParams, $location, $http, $timeout){
    $scope.artistInfo = [];
    $scope.currentScorePage = 1;
    $scope.currentWorkPage = 1;

    function findData(parent){

        query = $location.search();
        $http({
            url: "/api/artist.php", 
            method: "GET",
            params: {id: query.id}
        }).success(function(data, status, headers, config) {
            $scope.artistInfo = data;
            $timeout(function() {$('[data-toggle="tooltip"]').tooltip(); }, 1);
        });
    };

    // This resets the bootstrap tooltips containing the titles of the respective
    // pieces. These appear when the mouse hovers over the images, but they need
    // to be manually activated, so we do this whenever the page is changed for
    // the paginated data. View Caroline Norton's page to see this in action for
    // both categories.
    $scope.$watch('currentScorePage', function(newVal, oldVal) {
      $timeout(function() {$('[data-toggle="tooltip"]').tooltip(); }, 1);
    });
    $scope.$watch('currentWorkPage', function(newVal, oldVal) {
      $timeout(function() {$('[data-toggle="tooltip"]').tooltip(); }, 1);
    });

    // If multiple scores exist for a work, then we link to the page that displays those
    // multiple scores. Otherwise, we simply link to the score itself.
    $scope.findWorkPage = function(work) {
      if (work.score_count > 1)
      {
        return "/work.html#?id=" + work.id;
      }
      else
      {
        return "/score.html#?id=" + work.first_score_id;
      }
    }

    $scope.$on('$locationChangeStart', function(event) {
        findData(this);
    });
});

// A very basic controller for the work page. This page is used to display
// multiple scores that correspond to the same "work" of literature.
// E.g. we have several versions of "Auld Robin Gray"
app.controller("work_controller", function($scope, $routeParams, $location, $http){
    $scope.workInfo = [];
    function findData(parent){

        query = $location.search();
        $http({
            url: "/api/work.php", 
            method: "GET",
            params: {id: query.id}
        }).success(function(data, status, headers, config) {
            $scope.workInfo = data;
        });
    };

    $scope.$on('$locationChangeStart', function(event) {
        findData(this);
    });
});

// The controller for the publisher page. This page lists basic information about the publisher
// along with a description (if available), associated composers, and published scores.
app.controller("publisher_controller", function($scope, $routeParams, $location, $http, $timeout){
    $scope.pubInfo = [];
    $scope.currentScorePage = 1;

    // Manually enable the bootstrap tooltip just like for the artist page
    // when a new page of scores is viewed.
    $scope.$watch('currentScorePage', function(newVal, oldVal) {
      $timeout(function() {$('[data-toggle="tooltip"]').tooltip(); }, 1);
    });

    function findData(parent){

        query = $location.search();
        $http({
            url: "/api/publisher.php", 
            method: "GET",
            params: {id: query.id}
        }).success(function(data, status, headers, config) {
            $scope.pubInfo = data;
            $timeout(function() {$('[data-toggle="tooltip"]').tooltip(); }, 1);
        });
    };

    $scope.$on('$locationChangeStart', function(event) {
        findData(this);
    });
});

// The controller for the collections page, where we list information about a collection of scores
// such as a book and a (paginated) table of its contained scores.
app.controller("collection_controller", function($scope, $routeParams, $location, $http, $window){
    $scope.colInfo = []; 
    $scope.currentPage = 1;
    $scope.scoresPerPage = 20;

    // See on the html markup where we display
    // "Viewing results 1 - 20 out of X"
    // Needed to make the Math.min function available in the AngularJS scope
    // for this page in order to deal with viewing the last page.
    $scope.min = $window.Math.min;

    $scope.$watch('currentPage', function(newVal, oldVal) {
      scrollToTop();
    });

    function findData(parent){

        query = $location.search();
        $http({
            url: "/api/collection.php", 
            method: "GET",
            params: {id: query.id}
        }).success(function(data, status, headers, config) {
            $scope.colInfo = data;
        });
    };

    $scope.$on('$locationChangeStart', function(event) {
        findData(this);
    });
});

// The controllwer for the search page. This is also quite complicated compared to the
// other controllers because of the user input for this page.
// Important scope variables submitted to the api:
//   searchin - the category of our search: e.g. artists, collections, scores, works, or publishers
//   searchtype - what fields we are searching by, e.g. by first and last name for artist, by title for score, etc.
//   searchstring - the actual user submitted search string.
//   caveat - specific boolean options hard-coded in the backend for each searchin category
app.controller("search_controller", function($scope, $routeParams, $location, $http, $window){
    $scope.currentPage = 1;
    $scope.scoresPerPage = 20;

    $scope.searchin;
    $scope.searchstring;
    $scope.searchtype = DATA_SEARCH_TYPES;
    $scope.caveat = {
      score: {has_augnotes: false},
      artist: {is_composer: false, is_lyricist: false, has_biography: false},
      work: {multiple_scores: false},
      publisher: {has_description: false}
    };
    $scope.helpDescriptions = DATA_CATEGORY_DESCRIPTIONS;

    // Needed to make this available in the general anglar scope
    // to allow result count to be displayed correctly with pagination.
    // Just like in the collection page.
    $scope.min = $window.Math.min;

    // Scroll to top of page if the page is changed.
    $scope.$watch('currentPage', function(newVal, oldVal) {
      scrollToTop();
    });

    // Simple function to remove leading and trailing spaces from
    // a string - used so that the user can't just type a bunch of spaces
    // into the search bar and then submit.
    function myTrim(x) {
      return x.replace(/^\s+|\s+$/gm,'');
    }

    // Checks to make sure that one search field is true
    function searchtypeDefined()
    {
      var searchtype = $scope.searchtype[$scope.searchin];
      var searchtypeList = [];
      for (k in searchtype)
      {
        if (searchtype.hasOwnProperty(k))
        {
          searchtypeList.push(searchtype[k]);
        }
      }

      return (searchtypeList.indexOf(true) !== -1);
    }

    // This function makes sure that the user has selected a category
    // to search in, written a searchstring to search by, and has selected
    // searchtype fields to search by.
    function readyToSearch()
    {
      if ( myTrim($scope.searchin) !== "" && myTrim($scope.searchstring) !== "" && searchtypeDefined())
      {
        return true;
      }

      return false;
    }

    // This function reads a comma separated list of searchtypes or caveates
    // from the url to fill them in to the searchtype and caveat scope
    // variables appropriately.
    function constructParam(param)
    {
      var searchtype;
      if (param == "searchtype")
      {
        searchtype = $location.search().searchtype;
        scopevar = $scope.searchtype[$scope.searchin];
      }
      else if (param == "caveat")
      {
        searchtype = $location.search().caveat;
        scopevar = $scope.caveat[$scope.searchin];
      }


      if (searchtype && $scope.searchin !== "")
      {
        searchtypeArr = searchtype.split(',');

        for (var k in scopevar)
        {
          if (scopevar.hasOwnProperty(k))
          {
            if ($.inArray(k, searchtypeArr) !== -1)
            {
              scopevar[k] = true;
            }
            else
            {
              scopevar[k] = false;
            }
          }
        }
      }
    }

    // This function concatenates either $scope.searchtype
    // or $scope.caveat for the selected searchin value
    // to a comma separated list of selected searchtypes and caveats
    // in order to submit them via the URL to the PHP API
    function concatParam(param_object)
    {
      // Select searchtype for what we are searching in
      var paramArr = [];

      for (var k in param_object)
      {
        if (param_object.hasOwnProperty(k))
        {
          if (param_object[k])
          {
            paramArr.push(k);
          }
        }
      }

      return paramArr.join(',');
    }

    // Called to submit the search based on user input.
    // Adjusts the url appropriately and submits an AJAX request
    // to the search.php page.
    $scope.submitSearch = function() {
          if (!readyToSearch())
          {
            return;
          }
          $location.search('searchstring', $scope.searchstring);
          $location.search('searchin', $scope.searchin);
          $location.search('searchtype', concatParam($scope.searchtype[$scope.searchin]));

          var caveatStr = concatParam($scope.caveat[$scope.searchin]);
          if (caveatStr != '')
          {
            $location.search('caveat', caveatStr);
          }
          else
          {
            $location.search('caveat', null);
          }

          $scope.resultsLoading = true;
          $scope.searchedOnce = true;
          $scope.searchstring = myTrim($scope.searchstring);
          $.get(
            "/api/search.php", 
            {
              searchstring: $scope.searchstring.toLowerCase(),
              searchtype: concatParam($scope.searchtype[$scope.searchin]),
              searchin: $scope.searchin,
              caveat: concatParam($scope.caveat[$scope.searchin])
            },
            function( data ) { 
              $scope.results = data; 
              $scope.resultsLoading = false;
              $scope.currentPage = 1;
              $scope.$apply();
            },
            "json"
            );
    };

    // Used to determine where a search result item's title and image should
    // link to. Similarly to in the artist controller above,
    // we link directly to the score page for a corresponding work
    // if only one score exists.
    $scope.resourcePageLink = function(resourceId, searchin, result_item)
    {
      var pageTitle = "./";

      if (searchin != 'work') 
      {
        return pageTitle + searchin + ".html#?id=" + resourceId;
      }
      else 
      {
        if (result_item.data["Available Scores"] > 1) 
        {
          return "work.html#?id=" + resourceId;
        }
        else
        {
          return "score.html#?id=" + result_item.extra_data.first_score_id;
        }
      }
    }

    // Constructs the search based on URL querystring values when
    // the page is initially loaded so that someone can copy and paste
    // their search url to share with someone else or save for later.
    $scope.$on('$locationChangeStart', function(event) {
      var query = $location.search();

      $scope.searchin = (query.searchin ? query.searchin : "");
      $scope.searchstring = (query.searchstring ? query.searchstring : "");
      $scope.searchedOnce = false;
      $scope.resultsLoading = false;
      $scope.results = {};

      constructParam('searchtype');
      constructParam('caveat');

      if (readyToSearch())
      {
        $scope.submitSearch();
      }

      // Manually enable the Bootstrap popover component for displaying
      // the help information next to the search bar.
      $('[data-toggle="popover"]').popover();
    });
});

// The controller for the search on the main page of this website. This simply redirects
// to the main search page with all searchtypes selected for the selected category.
app.controller("index_search_controller", function($scope, $routeParams, $location, $http, $window){
    $scope.searchtype = DATA_SEARCH_TYPES;
    function myTrim(x) {
      return x.replace(/^\s+|\s+$/gm,'');
    }

    function readyToSearch()
    {
      if ( myTrim($scope.searchin) !== "" && myTrim($scope.searchstring) !== "")
      {
        return true;
      }

      return false;
    }

    function concatParam(param_object)
    {
      // Select searchtype for what we are searching in
      var paramArr = [];

      for (var k in param_object)
      {
        if (param_object.hasOwnProperty(k))
        {
          if (param_object[k])
          {
            paramArr.push(k);
          }
        }
      }

      return paramArr.join(',');
    }

    $scope.redirectSearch = function() {
      if (!readyToSearch())
      {
        return;
      }

      var search_url = '/search.html#?searchstring=' + $scope.searchstring;
      search_url += '&searchin=' + $scope.searchin;
      search_url += '&searchtype=' + concatParam($scope.searchtype[$scope.searchin]);

      $window.location.href = search_url;
    }

    $scope.$on('$locationChangeStart', function(event) {
      var query = $location.search();

      $scope.searchin = "score";
      $scope.searchstring = "";
    });
});

// This is a simple controller used to pull information about numbers of
// scores, artists, etc. to display in the about page for the website.
app.controller("info_controller", function($scope, $routeParams, $location, $http){
    $scope.genInfo = [];
    function findData(parent){

        query = $location.search();
        $http({
            url: "/api/info.php", 
            method: "GET",
        }).success(function(data, status, headers, config) {
            $scope.genInfo = data;
        });
    };

    $scope.$on('$locationChangeStart', function(event) {
        findData(this);
    });
});

// The controller for the browse page. This page actually uses the search API to pull
// a list of all records for each category (if no searchstring is provided, the search.php
// page provides all records for a given searchin value). In addition, the "has_augnotes" caveat
// is used for the "Performances" category, which is really just a refined browse scores category.
// For example, one could do the same thing to add "Composers" or "Poets" categories if this
// functionality is desired.
app.controller("browse_controller", function($scope, $routeParams, $location, $http, $window){

    $scope.currentPage = 1;
    $scope.helpDescriptions = DATA_CATEGORY_DESCRIPTIONS;

    // We are able to provide a caveat in addition to the searchin
    // category value (functionality needed to browse by "performance")
    $scope.findData = function(searchin_val, caveat_val) {
          $scope.resultsLoading = true;

          // Simply use the search API to get a list of all results!
          $.get(
            "/api/search.php", 
            {
              caveat: caveat_val,
              searchin: searchin_val
            },
            function( data ) { 
              $scope.results = data; 
              $scope.resultsLoading = false;
              $scope.$apply();
            },
            "json"
            );
    };

    // Called whever the user clicks on a different browse tab.
    $scope.browseNew = function(type) {
      $scope.browsein = type;
      $location.search('browsein', type);

      if (type == 'performance')
      {
        $scope.findData('score', 'has_augnotes');
      }
      else
      {
        $scope.findData($scope.browsein, "");
      }
      $scope.currentPage = 1;
    }

    // Scroll to top of page if the page is changed.
    $scope.$watch('currentPage', function(newVal, oldVal) {
      scrollToTop();
    });

    // Same functionality needed in the search page to determine where a
    // record should link.
    $scope.resourcePageLink = function(resourceId, searchin, result_item)
    {
      var pageTitle = "./";

      if (searchin != 'work' && searchin != 'performance') 
      {
        return pageTitle + searchin + ".html#?id=" + resourceId;
      }
      else if (searchin == 'performance')
      {
      	console.log("New page is here...")
      	return pageTitle + "score.html#?id=" + resourceId;
      }
      else
      {
        if (result_item.data["Available Scores"] > 1) 
        {
          return "work.html#?id=" + resourceId;
        }
        else if (result_item.extra_data)
        {
          return "score.html#?id=" + result_item.extra_data.first_score_id;
        }
      }


    }

    // The page pulls the browsein value from the URL so that
    // someone can link to a specific category externally.
    $scope.$on('$locationChangeStart', function(event) {
      var query = $location.search();

      $scope.browsein = (query.browsein ? query.browsein : "score");
      $scope.resultsLoading = false;
      $scope.results = {};

      $scope.browseNew($scope.browsein);
    });
});

})()